FROM node:alpine

ENV CHIX_PORT 2303
ENV CHIX_CLIENT_ID chix
ENV CHIX_MONGO_DATABASE chix
ENV CHIX_MONGO_URI mongodb://chix-mongo
ENV WHITELIST http://localhost,http://test.com
ENV CHIX_JWT_SECRET change_me
ENV ADMIN_USER john
ENV ADMIN_PASSWORD doe

ADD lib/ /app/lib
ADD package.json /app
ADD index.js /app

COPY DockerEntrypoint.sh /usr/local/bin/

WORKDIR /app

RUN apk --no-cache add --virtual native-deps \
  git g++ gcc libgcc libstdc++ linux-headers make python && \
  npm install node-gyp -g &&\
  npm install &&\
  npm rebuild bcrypt --build-from-source && \
  npm cache clean --force &&\
  apk del native-deps

RUN apk --no-cache add git openssh-client
RUN chown node:node /app -R

USER node

EXPOSE 2303

ENTRYPOINT ["DockerEntrypoint.sh"]
CMD ["npm", "run", "start:prod"]

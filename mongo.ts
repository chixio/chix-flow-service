import { MongoClient } from 'mongodb'

// Connection URL
const url = 'mongodb://localhost:27017'

// Database Name
const dbName = 'chix'

// Use connect method to connect to the server
MongoClient.connect(url, (_err, client) => {
  console.log('Connected successfully to server')

  const db = client.db(dbName)
  console.log('DB', db.databaseName)

  const cursor = db.collection('nodes').find({})

  console.log('CURSOR', (cursor as any).ns)

  db.collection('nodes')
    .find({})
    .map(async (document) => document.ownerId)
    .toArray()
    .then((result) => {
      console.log(result)
      client.close()
    })
})



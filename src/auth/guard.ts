import {
  CanActivate,
  Injectable,
  ExecutionContext
} from '@nestjs/common'
import { ErrorMessage } from '@nestling/errors'
import * as jwt from 'jsonwebtoken'
import { ConfigService } from '@nestling/config'
import { setContext } from '@nestling/context'

import { JWTConfig } from './types'
import { connectDb } from '../support/typeorm/connectDb'
import { AccessToken } from '../gogs/entities/access_token'
import { getRepository } from 'typeorm'
import { LogService } from '@nestling/logger'
import * as uuid from 'uuid'

@Injectable()
export class AuthGuard implements CanActivate {
  constructor (
    private config: ConfigService,
    private logger: LogService
  ) {}

  async canActivate (executionContext: ExecutionContext): Promise<boolean> {
    const {
      jwt: {
        contextKey = 'jwt',
        secret,
        header = 'authorization'
      }
    }: JWTConfig = this.config as any

    const httpContext = executionContext.switchToHttp()
    const request = httpContext.getRequest()
    const { headers } = request

    if (typeof headers[header] === 'string') {
      const parts = headers[header].split(' ')

      if (parts[0] === 'Bearer') {
        const token = parts[1]
        const decoded: any = jwt.verify(token, secret)

        setContext(contextKey, decoded, request)
        setContext('Authorization', headers[header], request)
        setContext('logger', this.logger.child({ reqId: uuid.v4(), uid: decoded.sub }), request)

        return true
      }

      if (parts[0] === 'token') {
        await connectDb()
        const accessTokenRepository = getRepository(AccessToken)

        const accessToken = await accessTokenRepository.findOne({ sha1: parts[0] })

        if (accessToken) {
          setContext('token', accessToken, request)
          setContext('Authorization', headers[header], request)

          setContext('logger', this.logger.child({ reqId: uuid.v4(), uid: accessToken.uid || 'nouid' }), request)

          return true
        }
      }
    }

    // authorization is done by User(required: true) decorator
    return true
    // throw new ErrorMessage('auth:unauthorized')
  }
}

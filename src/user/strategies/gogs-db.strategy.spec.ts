import { Test } from '@nestjs/testing'
import { LoggerModule } from '@nestling/logger'
import { ConfigModule } from '@nestling/config'
import { expect } from 'chai'
import {
  after,
  before,
  describe,
  it
} from 'mocha'
import { ErrorMessage } from '@nestling/errors'
import { userErrors } from '../user.errors'
import { GogsDbStrategy } from './gogs-db.strategy'
import { createTestApp, TestContext } from '../../support/createTestApp'
import { getConnection } from 'typeorm'

ErrorMessage.addErrorMessages(userErrors)

describe('GogsDbStrategy', () => {
  let gogsDbStrategy: GogsDbStrategy
  let testApp: TestContext

  before(async () => {
    testApp = await createTestApp('nodes')

    gogsDbStrategy = testApp.app.get<GogsDbStrategy>(GogsDbStrategy)
  })

  after(async () => {
    await gogsDbStrategy.stop()
    await testApp.close()
  })

  it('should start', async () => {
    await gogsDbStrategy.start()
  })

  it('Can get user by id', async () => {
    const user = await gogsDbStrategy.getUser(testApp.user.id)

    expect(user.id).to.equal(testApp.user.id)
    expect(user.name).to.equal(testApp.user.name)
  })

  it('Can get user by name', async () => {
    const user = await gogsDbStrategy.getUserByName(testApp.user.name)

    expect(user.id).to.equal(testApp.user.id)
    expect(user.name).to.equal(testApp.user.name)
  })

  it('Can preload', async () => {
    await gogsDbStrategy.preload()

    expect(gogsDbStrategy.users[testApp.user.id]).to.deep.equal({
      id: testApp.user.id,
      name: testApp.user.name
    })

    expect(gogsDbStrategy.users[testApp.user2.id]).to.deep.equal({
      id: testApp.user2.id,
      name: testApp.user2.name
    })
  })

  it('should stop', async () => {
    await gogsDbStrategy.stop()
  })
})

import { getRepository, Repository } from 'typeorm'

import {
  Inject,
  Injectable
} from '@nestjs/common'
import { LogService } from '@nestling/logger'
import { ErrorMessage } from '@nestling/errors'

import { UserModel } from '@chix/common'
import { UserStrategyInterface } from './user.strategy.interface'
import { StartableInterface } from '../../interfaces'
import { User } from '../../gogs/entities/user.entity'

export type CachedUserModels = {
  [id: number]: UserModel
}

@Injectable()
export class GogsDbStrategy implements UserStrategyInterface, StartableInterface {
  userRepository: Repository<User>

  users: CachedUserModels = {}
  constructor (
    private log: LogService
  ) {}

  async start () {
    this.log.info('GogsDbStrategy Start.')

    this.userRepository = getRepository(User)
  }

  async stop () {
    /*
    if (this.connection) {
      await this.connection.close()
    }
    */
  }

  async preload (): Promise<void> {
    this.log.info('preloading user information')

    await this.cacheUserModels()
  }

  private async cacheUserModels () {
    const users = await this.userRepository.find()

    this.users = users.reduce((_users, user) => {
      _users[user.id] = {
        id: user.id,
        name: user.name
      }

      _users[user.name] = {
        id: user.id,
        name: user.name
      }

      return _users
    }, {})

  }

  async getUserByName (name: string): Promise<UserModel> {
    let user = this.users[name]

    if (!user) {
      const result = await this.userRepository.findOne({ name })

      if (result) {
        user = {
          id: result.id,
          name: result.name
        }
      }
    }

    if (user) {
      this.log.info('Found user information.')
      this.log.info(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }

  async getUser (id: number): Promise<UserModel> {
    let user = this.users[id]

    if (!user) {
      const result = await this.userRepository.findOne({ id })

      if (result) {
        user = {
          id: result.id,
          name: result.name
        }
      }
    }

    if (user) {
      this.log.info('Found user information.')
      this.log.info(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }
}

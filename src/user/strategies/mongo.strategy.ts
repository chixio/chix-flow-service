import {
  Inject,
  Injectable
} from '@nestjs/common'
import { LogService } from '@nestling/logger'
import { ErrorMessage } from '@nestling/errors'
import { ConfigService } from '@nestling/config'

import { UserId, UserModel } from '@chix/common'
import { UserStrategyInterface } from './user.strategy.interface'
import { StartableInterface } from '../../interfaces/startable.interface'

export type CachedUserModels = {
  [id: string]: UserModel
}

@Injectable()
export class MongoStrategy implements UserStrategyInterface, StartableInterface {
  db
  users: CachedUserModels = {}
  constructor (
    private log: LogService,
    private config: ConfigService,
    @Inject('MongoDbToken') public readonly mongo
  ) {}

  async start () {
    this.log.info('MongoDB Adapter, selected database')
    this.db = await this.mongo.db(
      (this.config as any).user.options.database
    )

    // TODO: watch change stream, and reload this.users

    /* Implement later. Only works on replica sets.
    const changeStream = this.db.collection('users').watch()

    changeStream.next(function(err, next) {
      if (err) return console.log(err)

      console.log('NEW CHANGE', next)
      this.cacheUserModels()
    })
    */

    return
  }

  async stop () {
    return
  }

  async preload (): Promise<void> {
    this.log.info('preloading user information')

    await this.cacheUserModels()
  }

  private async cacheUserModels () {
    const collection = this.db.collection('users')

    const users = await collection.find({}, {
      _id: 0,
      id: 1,
      username: 1
    }).toArray()

    this.users = users.reduce((_users, user) => {
      _users[user.id] = {
        id: user.id,
        name: user.username
      }

      _users[user.username] = {
        id: user.id,
        name: user.username
      }

      return _users
    }, {})

  }

  async getUserByName (name: string): Promise<UserModel> {
    let user = this.users[name]

    if (!user) {
      const result = await this.db.collection('users').findOne({ username: name })

      if (result) {
        user = {
          id: result.id,
          name: result.username
        }
      }
    }

    if (user) {
      this.log.info('Found user information.')
      this.log.info(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }

  async getUser (id: UserId): Promise<UserModel> {
    let user = this.users[id]

    if (!user) {
      const result = await this.db.collection('users').findOne({ id })

      if (result) {
        user = {
          id: result.id,
          name: result.username
        }
      }
    }

    if (user) {
      this.log.info('Found user information.')
      this.log.info(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }
}

import { UserModel } from '@chix/common'

export interface UserStrategyInterface {
  preload (): Promise<void>
  getUser (id: number): Promise<UserModel>
  getUserByName (name: string): Promise<UserModel>
}

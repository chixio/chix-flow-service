import { Injectable } from '@nestjs/common'
import { LogService } from '@nestling/logger'
import { UserId, UserModel } from '@chix/common'
import { Observable } from 'rxjs'
import { MemcachedService } from '../../memcached'
import { UserStrategyInterface } from './user.strategy.interface'
import { StartableInterface } from '../../interfaces'
import { ErrorMessage } from '@nestling/errors'
import * as caller from 'grpc-caller'
import { resolve } from 'path'
import { ConfigService } from '@nestling/config'

// TODO: duplicate from flow-service, all already defined there
export interface GrpcUserService {
  Preload (params: any): Observable<UserModel[]>
  GetUsername (params: any): Observable<UserModel>
  GetId (params: any): Observable<UserModel>
}

@Injectable()
export class GrpcStrategy implements UserStrategyInterface, StartableInterface {
  private grpcUserService: any

  constructor (
    private log: LogService,
    private config: ConfigService,
    private memcachedService: MemcachedService
  ) {}

  async start (): Promise<void> {
    await this.memcachedService.start()

    const { options } = (this.config as any).user

    this.grpcUserService = caller(
      `${options.host}:${options.port}`,
      resolve(__dirname, '../user.proto'),
      'UserService'
    )

    return
  }

  async stop (): Promise<void> {
    return
  }

  async preload (): Promise<void> {
    this.log.info('preloading user information')

    const users = await this.grpcUserService.Preload({})

    // fill memcached.
    await Promise.all(
      users.map((user) => this.memcachedService.add(user.id.toString(), user))
        .concat(
          users.map((user) => this.memcachedService.add(user.name, user)
          )
        )
    )
    this.log.info('cached all current user information')
  }

  async getUserByName (name: string): Promise<UserModel> {
    this.log.info(`Retrieving user information for ${name}.`)

    let user = await this.memcachedService.get(name)

    if (!user) {
      user = await this.grpcUserService.GetId({ name })
    }

    if (user) {
      this.log.info(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }

  async getUser (id: UserId): Promise<UserModel> {
    this.log.info('Retrieving user information by id.')

    let user = await this.memcachedService.get(id.toString())

    if (!user) {
      user = await this.grpcUserService.GetUsername({ id })
    }

    if (user) {
      this.log.info(user)

      return user
    }

    throw new ErrorMessage('user:notfound')
  }
}

import { createParamDecorator } from '@nestjs/common'
import { UserModel } from '@chix/common'
import { getContext } from '@nestling/context'
import { ErrorMessage } from '@nestling/errors'
import { userErrors } from './user.errors'
import { JWTModel } from '../jwt.model'

ErrorMessage.addErrorMessages(userErrors)

export interface UserDecoratorOptions {
  required?: boolean
}

export const User = createParamDecorator((options: UserDecoratorOptions, request): UserModel | null => {
  const decoded = getContext<JWTModel>('jwt', request)

  if (decoded) {
    const { sub, username } = decoded

    if (sub && username) {
      return {
        id: parseInt(sub, 10),
        name: username
      }
    }
  }

  if (options.required) {
    // throw new ErrorMessage('user:invalid')
    throw new ErrorMessage('auth:unauthorized')
  }

  return null
})

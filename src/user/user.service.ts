import { Inject, Injectable, OnModuleInit } from '@nestjs/common'

import { ErrorMessage } from '@nestling/errors'
import { LogService } from '@nestling/logger'

import {
  UserId,
  UserModel
} from '@chix/common'

import { UserStrategyInterface } from './strategies/user.strategy.interface'
import { StartableInterface } from '../interfaces'

@Injectable()
export class UserService implements OnModuleInit {

  constructor (
    private log: LogService,
    @Inject('UserStrategy') private strategy: UserStrategyInterface & StartableInterface
  ) {
    // todo, see why onModuleInit doesn't run on specific tests. (does run as application)
    // this.strategy.start()
  }

  async onModuleInit () {
    await this.strategy.start()
  }

  async preload (): Promise<void> {
    this.log.info('preloading user information')

    await this.strategy.preload()
  }

  async getUserByName (name: string): Promise<UserModel> {
    const user = await this.strategy.getUserByName(name)

    if (user) {
      return user
    }

    throw new ErrorMessage('user:notfound')
  }

  async getUser (id: UserId): Promise<UserModel> {
    const user = await this.strategy.getUser(id)

    if (user) {
      return user
    }

    throw new ErrorMessage('user:notfound')
  }
}

import { Module } from '@nestjs/common'
import { MemcachedService } from './memcached.service'
import { ConfigModule } from '@nestling/config'
import { LoggerModule } from '@nestling/logger'

@Module({
  imports: [
    ConfigModule,
    LoggerModule
  ],
  exports: [
    MemcachedService
  ],
  providers: [
    MemcachedService
  ]
})
export class MemcachedModule {}

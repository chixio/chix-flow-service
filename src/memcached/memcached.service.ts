import { Injectable, OnModuleDestroy } from '@nestjs/common'
import { LogService } from '@nestling/logger'
import { ConfigService } from '@nestling/config'
import * as Memcached from 'memcached'
import { StartableInterface } from '../interfaces'

@Injectable()
export class MemcachedService implements StartableInterface, OnModuleDestroy {
  private client!: Memcached
  constructor (
    private log: LogService,
    private config: ConfigService
  ) {}

  private cacheLifetime: number = 60 * 60

  async start () {
    this.client = new Memcached(
      (this.config as any).memcached.servers,
      {
        maxKeySize: 250, // the maximum key size allowed.
        maxExpiration: 2592000, // the maximum expiration time of keys (in seconds).
        maxValue: 1048576, // the maximum size of a value.
        poolSize: 10, // the maximum size of the connection pool.
        algorithm: 'md5', // the hashing algorithm used to generate the hashRing values.
        reconnect: 18000000, // the time between reconnection attempts (in milliseconds).
        timeout: 5000, // the time after which Memcached sends a connection timeout (in milliseconds).
        retries: 5, // the number of socket allocation retries per request.
        failures: 5, // the number of failed-attempts to a server before it is regarded as 'dead'.
        retry: 30000, // the time between a server failure and an attempt to set it up back in service.
        remove: false, // if true, authorizes the automatic removal of dead servers from the pool.
        failOverServers: undefined, // an array of server_locations to replace servers that fail and that are removed from the consistent hashing scheme.
        keyCompression: true, // whether to use md5 as hashing scheme when keys exceed maxKeySize.
        idle: 5000 // the idle timeout for the connections.
      }
    )

    this.client.on('failure', (details) => {
      this.log.error(
        `Server ${details.server} went down due to: ${details.messages.join(',')}`
      )
    })

    this.client.on('reconnecting', (details) => {
      this.log.debug(
        `Total downtime caused by server ${details.server}: ${details.totalDownTime}ms`
      )
    })

    this.log.info('Started memcache client.')
  }

  async stop () {
    if (this.client) {
      this.client.end()
    }
  }

  onModuleDestroy () {
    this.stop()
  }

  del (key: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.client.del(key, (error, data) => {
        if (error) {
          reject(error)
        } else {
          resolve(data)
        }
      })
    })
  }

  get (key: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.client.get(key, (error, data) => {
        if (error) {
          reject(error)
        } else {
          resolve(data)
        }
      })
    })
  }

  set (key: string, value: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.client.set(key, value, this.cacheLifetime, (error, data) => {
        if (error) {
          reject(error)
        } else {
          resolve(data)
        }
      })
    })
  }

  add (key: string, value: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.client.add(key, value, this.cacheLifetime, (error, data) => {
        if (error) {
          reject(error)
        } else {
          resolve(data)
        }
      })
    })
  }

  flush (): Promise<any> {
    return new Promise((resolve, reject) => {
      this.client.flush((error, data) => {
        if (error) {
          reject(error)
        } else {
          resolve(data)
        }
      })
    })
  }
}

import { FlowSchema, NodeSchema, ProjectSchema } from '@chix/common'
import { Module } from '@nestjs/common'
import { DatabaseModule } from '../database'
import { ValidatorModule } from '@nestling/validator'
import { LoggerModule } from '@nestling/logger'
import { ConfigModule } from '@nestling/config'
import { environment } from '../environment/environment'
import * as fs from 'fs'
import * as path from 'path'
import { DatabaseAdapterModule } from '../adapter'
import { UserModule } from '../user/user.module'
import { LoggerMiddleware } from './logger.middleware'
import { SharedService } from './shared.service'

const chixSchema = JSON.parse(
  fs.readFileSync(
    path.resolve(__dirname, './json-schema-draft-07-chix.json'),
    'utf8'
  )
)

const Node: any = {
  ...NodeSchema,
  $id: 'node.json',
  $schema: 'http://json-schema.org/draft-07-chix/schema#'
}

const Map: any = {
  ...FlowSchema,
  $id: 'flow.json',
  $schema: 'http://json-schema.org/draft-07-chix/schema#'
}

// common package format..
const Project: any = {
  ...ProjectSchema,
  $id: 'project.json',
  $schema: 'http://json-schema.org/draft-07-chix/schema#'
}

const Runtime: any = {
  $id: 'runtime.json',
  $schema: 'http://json-schema.org/draft-07-chix/schema#',
  type: 'object',
  properties: {
    _id: {
      type: 'string',
      required: false
    },
    id: {
      type: 'string',
      required: false
    },
    address: {
      type: 'string'
    },
    protocol: {
      type: 'websocket'
    },
    type: {
      type: 'string',
      enum: ['chix', 'chix-nodejs', 'noflo', 'noflo-nodejs']
    },
    label: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
    registered: {
      type: 'number',
      required: false
    },
    seen: {
      type: 'number',
      required: false
    },
    user: {
      type: 'string'
    },
    secret: {
      type: 'string'
    }
  }
}

const schemas = [
  Node,
  Map,
  Project,
  Runtime
]

const validators = []

const configModule = ConfigModule.forRoot(environment, {
  prefix: process.env.OBJENV_PREFIX || ''
})

@Module({
  imports: [
    LoggerModule.forRoot({
      name: environment.client.id || 'chix'
    }),
    configModule,
    DatabaseModule,
    DatabaseAdapterModule,
    UserModule,
    ValidatorModule.forRoot({
      schemas,
      validators,
      metaSchemas: [
        chixSchema
      ]
      /*
      inject: [
        mongoProvider
      ]
      */
    })
  ],
  exports: [
    configModule,
    DatabaseModule,
    DatabaseAdapterModule,
    UserModule,
    SharedService
  ],
  providers: [
    LoggerMiddleware,
    SharedService
  ]
})
export class SharedModule {}

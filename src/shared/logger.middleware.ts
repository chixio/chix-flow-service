import {
  Injectable,
  NestMiddleware,
  MiddlewareFunction,
  Request,
  Response
} from '@nestjs/common'
import { LogService } from '@nestling/logger'
import { setContext } from '@nestling/context'

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  constructor (
    private logger: LogService
  ) {}
  resolve (): MiddlewareFunction {
    return (request: Request, _response: Response, next) => {
      this.logger.info(request)

      setContext('logger', this.logger, request)

      if (next) next()
    }
  }
}

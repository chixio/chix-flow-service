import { Injectable } from '@nestjs/common'
import { LogService } from '@nestling/logger'
import { UserModel } from '@chix/common'
import { MongoDbAdapter } from '../adapter/mongo.adapter'

@Injectable()
export class SharedService {
  constructor (
    private log: LogService,
    private adapter: MongoDbAdapter
  ) { }

  async projectExists (user: UserModel, where: any = {}) {
    const _where: any = {
      providerId: user.id,
      ...where
    }

    this.log.info(`Test if exists: ${JSON.stringify(_where)}`)

    return this.adapter.exists('projects', _where)
  }
}

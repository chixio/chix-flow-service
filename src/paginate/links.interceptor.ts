import {
  Injectable,
  ExecutionContext,
  NestInterceptor
} from '@nestjs/common'
import { getContext } from '@nestling/context'
import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'
import { createPaginationLinks } from './createPaginationLinks'
import { PaginationContext } from '../adapter/mongo.adapter'

@Injectable()
export class LinksInterceptor implements NestInterceptor {
  intercept (
    executionContext: ExecutionContext,
    call$: Observable<any>
  ): Observable<any> {
    return call$
      .pipe(
        tap(() => {
          const httpContext = executionContext.switchToHttp()

          const request = httpContext.getRequest()
          const response = httpContext.getResponse()

          const pagination = getContext<PaginationContext>('pagination', request)

          if (pagination) {
            const paginationLinks = createPaginationLinks(
              pagination,
              request.path,
              request.params
            )

            if (paginationLinks.length) {
              response.setHeader('Link', paginationLinks.join(', '))
            }
          }
        })
      )
  }
}

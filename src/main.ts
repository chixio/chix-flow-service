import { NestFactory } from '@nestjs/core'
import { ApplicationModule } from './app.module'
import { environment } from './environment/environment'
import { whitelist } from '@nestling/cors'
import * as bodyParser from 'body-parser'
import { connectDb } from './support/typeorm/connectDb'
import { ensureTestUser } from './support/typeorm'
import { LogService } from '@nestling/logger';
import { ConfigService } from '@nestling/config';

async function bootstrap () {
  await connectDb()

  if (process.env.ENV === 'test') {
    const testUser = await ensureTestUser()

    console.log(`Test Mode, test user is:`, testUser)
  }

  const app = await NestFactory.create(ApplicationModule)

  const config = app.get<ConfigService>(ConfigService)
  app.get(LogService).level((config as any).log.level || 'info')

  app.use(bodyParser.json({
    limit: '100mb'
  }))

  app.enableCors(whitelist(environment.whitelist))

	// TODO: cannot get access to config service..
  await app.listen(environment.port || 2303)
}
bootstrap()

import { ContextModel } from '@nestling/context'
import { QueryOptions } from './mongo.adapter'

export interface WithId {
  id: string
}

export interface IDbAdapter {
  find: <T>(
    collection: string,
    by,
    options: QueryOptions,
    context?: ContextModel
  ) => Promise<T[]>
  findOne: <T>(collection, by) => Promise<T>
  insert: <T>(collection, item: T) => Promise<T>
  findById: <T>(collection, id: string) => Promise<T>
  update: <T extends WithId>(collection, item: T) => Promise<T>
}

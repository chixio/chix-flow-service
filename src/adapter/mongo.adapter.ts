import { IDbAdapter, WithId } from './IDbAdapter'
import { Injectable, Inject, OnModuleInit } from '@nestjs/common'

import { ConfigService } from '@nestling/config'
import { ContextModel } from '@nestling/context'

import {
  Collection,
  Cursor,
  Db,
  WriteOpResult
} from 'mongodb'
import * as uuid from 'uuid'

export type PaginationContext = {
  page: number
  perPage: number
  total: number
}

export type QueryOptions = {
  page?: number
  perPage?: number
}

export type UserFilter = {
  providerId?: string
}

export type DocFilters = {
  [collection: string]: DocFilter[]
}

export type DocFilter = <T> (document: T) => Promise<T>

export type FilterModel = UserFilter

@Injectable()
export class MongoDbAdapter implements IDbAdapter, OnModuleInit {
  db: Db

  private filters: DocFilters = {}

  constructor (
    @Inject('MongoDbToken') private readonly mongo,
    private config: ConfigService
  ) {
    this.db = this.mongo.db((this.config as any).mongo.database)
  }

  onModuleInit () {
    this.db = this.mongo.db((this.config as any).mongo.database)
  }

  async paginate (
    query: Cursor,
    context: ContextModel,
    page: number | undefined,
    perPage: number | undefined
  ): Promise<Cursor> {
    const total = await query.clone().count()

    context.set('pagination', {
      total,
      page,
      perPage
    })

    if (page === undefined) {
      // return this.transformMany<T>(query)
      return query
    }

    const limit = perPage || 10

    return query
      .skip(limit * (page - 1))
      .limit(limit)
  }

  private transformMany<T> (cursor: Cursor): Promise<T[]> {
    const ns = (cursor as any).ns

    if (!ns) throw Error('Could not determine ns')

    return new Promise((resolve, _reject) => {
      const docPromises: Array<Promise<T>> = []

      cursor.on('data', (doc: T) => {
        // here can be the async map function.
        // first let the tests just pass.
        docPromises.push(this.docFilter<T>(doc, ns))
      })

      cursor.on('end', () => {
        resolve(
          Promise.all(docPromises)
        )
      })
    })
  }

  private async transform<T> (doc: T, ns: string): Promise<T> {
    if (doc === null) {
      return doc
    }

    return this.docFilter<T>(doc, ns)
  }

  private async docFilter<T> (document: T, ns: string): Promise<T> {
    if (this.filters[ns] && this.filters[ns].length) {
      let doc = document

      for (const filter of this.filters[ns]) {
        doc = await filter(doc)
      }

      return doc
    }

    return document

  }

  addDocFilterFor (collection: string, filter: DocFilter) {
    const ns = `${this.db.databaseName}.${collection}`

    if (!this.filters[ns]) {
      this.filters[ns] = []
    }

    this.filters[ns].push(filter)
  }

  async find<T> (
    collection: string,
    by: any = {},
    options: QueryOptions = {},
    context?: ContextModel
  ): Promise<T[]> {
    let cursor = this.db
      .collection(collection)
      .find(by, {
        projection: {
          // providerId: 0,
          _id: 0
        }
      })

    if (options.page && options.perPage) {
      if (!context) {
        throw Error('Paging requires context to be set')
      }
      cursor = await this.paginate(
        cursor,
        context,
        options.page,
        options.perPage
      )
    }

    return this.transformMany<T>(cursor)
  }

  /**
   * Search collection
   *
   * Note: expects a search index to be defined on the collection.
   */
  async search<T> (
    context: ContextModel,
    collection: string,
    $search,
    options: QueryOptions = {},
    where: any = {}
  ): Promise<T[]> {
    if (!$search) {
      return []
    }
    let cursor = this.db.collection(collection)
      .find(
        { $text: { $search } },
        {
          score: { $meta: 'textScore' },
          ...where
        } as any
      )

    cursor = await this.paginate(
      cursor,
      context,
      options.page,
      options.perPage
    )

    return this.transformMany<T>(cursor)
    // .sort({ score: { $meta: 'textScore' } }).toArray()
  }

  async findOne<T> (collection, by): Promise<T> {
    return this.transform(
      await this.db
        .collection(collection)
        .findOne(by, {
          projection: {
            // providerId: 0,
            _id: 0
          }
        }),
      `${this.db.databaseName}.${collection}`
    )
  }

  async count(collection, by: any = {}): Promise<number> {
    return this.db
      .collection(collection)
      .find(by)
      .count()
  }

  async exists (collection, by): Promise<boolean> {
    const count = await this.count(collection, by)

    return count > 0
  }

  async remove (collection, id): Promise<boolean> {
    const { result } = await this.db
      .collection(collection)
      .remove({
        id
      })

    return Boolean(result.ok)
  }

  async removeAll (collection, where: any): Promise<boolean> {
    const { result } = await this.db
      .collection(collection)
      .remove({
        ...where
      })

    return Boolean(result.ok)
  }

  async insert<T> (collection, item: Partial<T>): Promise<T> {
    const col: Collection = await this.db.collection(collection)

    const id = (item as any).id ? (item as any).id : uuid.v4()

    const doc = { id, ...item as Object }

    const { result }: any = await col.insertOne(doc)

    if (result.ok) {
      return this.findById(collection, id) as Promise<T>
    }

    throw Error(`Failed to insert item into ${collection}`)
  }

  async findById<T> (collection, id: string): Promise<T> {
    const col: Collection = await this.db.collection(collection)

    const document = await col.findOne({ id }, {
      projection: {
        // providerId: 0,
        _id: 0
      }
    })

    return this.transform(
      document,
      `${this.db.databaseName}.${collection}`
    )
  }

  async update<T extends WithId> (collection, item: T): Promise<T> {
    const col: Collection = await this.db.collection(collection)

    let _item

    if (item.id) {
      _item = await col.findOne({ id: item.id })
    }

    const excludeFromUpdate: string[] = [
      'id',
      '_id'
    ]

    let hasChanges: boolean = false

    const $set = Object.keys(item).reduce((updates, key) => {
      if (
        excludeFromUpdate.indexOf(key) === -1 &&
        (!_item || _item[key] !== item[key])
      ) {
        hasChanges = true
        updates[key] = item[key]
      }

      return updates
    }, {})

    const id = item.id || uuid.v4() // possible upsert

    if (hasChanges) {
      await col.update({
        id
      }, {
        $set
      }, {
        upsert: true
      })
    }

    return this.findById(collection, id) as Promise<T>
  }

  async flush (collection): Promise<WriteOpResult> {
    return this.db
      .collection(collection)
      .remove({})
  }

  async close () {
    return this.mongo.close()
  }
}

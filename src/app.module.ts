import { Module } from '@nestjs/common'
import { APP_FILTER, APP_GUARD } from '@nestjs/core'
import { FlowModule } from './modules/flows'
import { NodeModule } from './modules/nodes'
import { ProjectModule } from './modules/projects'
import { RuntimeModule } from './modules/runtimes'
import { SharedModule } from './shared/shared.module'

import { HttpExceptionFilter, ErrorMessage } from '@nestling/errors'
import { validationErrorHandler } from '@nestling/errors/dist/handlers/validationError'
import { jsonWebTokenErrorHandler } from '@nestling/errors/dist/handlers/jsonWebTokenError'
import { PaginateModule } from './paginate'
import { AuthGuard, authErrors } from './auth'

HttpExceptionFilter.addExceptionHandler(validationErrorHandler)
HttpExceptionFilter.addExceptionHandler(jsonWebTokenErrorHandler)
ErrorMessage.addErrorMessages(authErrors)

@Module({
  imports: [
    SharedModule,
    PaginateModule,
    FlowModule,
    NodeModule,
    ProjectModule,
    RuntimeModule
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard
    },
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter
    }
  ]
})
export class ApplicationModule {}

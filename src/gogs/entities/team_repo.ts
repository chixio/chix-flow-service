import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('team_repo',{ schema: 'chix_gogs' })
@Index('UQE_team_repo_s',['teamId','repoId'],{ unique: true })
@Index('IDX_team_repo_org_id',['orgId'])
export class TeamRepo {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'org_id'
  })
  orgId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'team_id'
  })
  teamId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null
}

import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('deploy_key',{ schema: 'chix_gogs' })
@Index('UQE_deploy_key_s',['keyId','repoId'],{ unique: true })
@Index('IDX_deploy_key_key_id',['keyId'])
@Index('IDX_deploy_key_repo_id',['repoId'])
export class DeployKey {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'key_id'
  })
  keyId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name'
  })
  name: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'fingerprint'
  })
  fingerprint: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix'
  })
  updated: string | null

}

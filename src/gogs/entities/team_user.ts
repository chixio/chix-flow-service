import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('team_user',{ schema: 'chix_gogs' })
@Index('UQE_team_user_s',['teamId','uid'],{ unique: true })
@Index('IDX_team_user_org_id',['orgId'])
export class TeamUser {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'org_id'
  })
  orgId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'team_id'
  })
  teamId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'uid'
  })
  uid: string | null

}

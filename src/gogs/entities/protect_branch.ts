import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('protect_branch',{ schema: 'chix_gogs' })
@Index('UQE_protect_branch_protect_branch',['repoId','name'],{ unique: true })
export class ProtectBranch {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name'
  })
  name: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'protected'
  })
  protected: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'require_pull_request'
  })
  requirePullRequest: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'enable_whitelist'
  })
  enableWhitelist: boolean | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'whitelist_user_i_ds'
  })
  whitelistUuserIds: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'whitelist_team_i_ds'
  })
  whitelistTeamIds: string | null
}

import {
  Column,
  Entity,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('two_factor_recovery_code',{ schema: 'chix_gogs' })
export class TwoFactorRecoveryCode {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'user_id'
  })
  userId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 11,
    name: 'code'
  })
  code: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_used'
  })
  isUsed: boolean | null
}

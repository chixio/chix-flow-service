import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('pull_request',{ schema: 'chix_gogs' })
@Index('IDX_pull_request_issue_id',['issueId'])
export class PullRequest {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'int',
    nullable: true,
    name: 'type'
  })
  type: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'status'
  })
  status: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'issue_id'
  })
  issueId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'index'
  })
  index: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'head_repo_id'
  })
  headRepoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'base_repo_id'
  })
  baseRepoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'head_user_name'
  })
  headUserName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'head_branch'
  })
  headBranch: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'base_branch'
  })
  baseBranch: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 40,
    name: 'merge_base'
  })
  mergeBase: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'has_merged'
  })
  hasMerged: boolean | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 40,
    name: 'merged_commit_id'
  })
  mergedCommitId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'merger_id'
  })
  mergerId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'merged_unix'
  })
  mergedUnix: string | null
}

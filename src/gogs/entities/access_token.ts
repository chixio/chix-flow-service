import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('access_token',{ schema: 'chix_gogs' })
@Index('UQE_access_token_sha1',['sha1'],{ unique: true })
@Index('IDX_access_token_uid',['uid'])
export class AccessToken {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'uid'
  })
  uid: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name'
  })
  name: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    unique: true,
    length: 40,
    name: 'sha1'
  })
  sha1: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix'
  })
  updated: string | null

}

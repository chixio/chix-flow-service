import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('comment',{ schema: 'chix_gogs' })
@Index('IDX_comment_issue_id',['issueId'])
export class Comment {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'int',
    nullable: true,
    name: 'type'
  })
  type: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'poster_id'
  })
  posterId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'issue_id'
  })
  issueId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'commit_id'
  })
  commitId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'line'
  })
  line: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'content'
  })
  content: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix'
  })
  updated: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 40,
    name: 'commit_sha'
  })
  commitSha: string | null

}

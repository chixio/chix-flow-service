import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('team',{ schema: 'chix_gogs' })
@Index('IDX_team_org_id',['orgId'])
export class Team {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'org_id'
  })
  orgId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'lower_name'
  })
  lowerName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name'
  })
  name: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'description'
  })
  description: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'authorize'
  })
  authorize: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_repos'
  })
  numRepos: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_members'
  })
  numMembers: number | null
}

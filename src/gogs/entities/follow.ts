import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('follow',{ schema: 'chix_gogs' })
@Index('UQE_follow_follow',['userId','followId'],{ unique: true })
export class Follow {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'user_id'
  })
  userId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'follow_id'
  })
  followId: string | null

}

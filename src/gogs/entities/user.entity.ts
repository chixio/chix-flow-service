import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

export type UserType = number

export const USER_TYPE_INDIVIDUAL = 0
export const USER_TYPE_ORGANIZATION = 1

@Entity('user',{ schema: 'chix_gogs' })
@Index('UQE_user_name',['name'],{ unique: true })
@Index('UQE_user_lower_name',['lowerName'],{ unique: true })
export class User {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: number

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
    length: 255,
    name: 'lower_name'
  })
  lowerName: string

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
    length: 255,
    name: 'name'
  })
  name: string

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'full_name'
  })
  fullName: string | null

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'email'
  })
  email: string

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'passwd'
  })
  passwd: string

  @Column({
    type: 'int',
    nullable: true,
    name: 'login_type'
  })
  loginType: number | null

  @Column({
    type: 'bigint',
    nullable: false,
    default: 0,
    name: 'login_source'
  })
  loginSource: number

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'login_name'
  })
  loginName: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'type'
  })
  type: number | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'location'
  })
  location: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'website'
  })
  website: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 10,
    name: 'rands'
  })
  rands: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 10,
    name: 'salt'
  })
  salt: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix'
  })
  updated: number | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'last_repo_visibility'
  })
  lastRepoVisibility: boolean | null

  @Column({
    type: 'int',
    nullable: false,
    default: '-1',
    name: 'max_repo_creation'
  })
  maxRepoCreation: number

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_active'
  })
  isActive: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_admin'
  })
  isAdmin: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'allow_git_hook'
  })
  allowGitHook: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'allow_import_local'
  })
  allowImportLocal: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'prohibit_login'
  })
  prohibitLogin: boolean | null

  @Column({
    type: 'varchar',
    nullable: false,
    length: 2048,
    name: 'avatar'
  })
  avatar: string

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'avatar_email'
  })
  avatarEmail: string

  @Column({
    type: 'int',
    nullable: true,
    width: 1,
    name: 'use_custom_avatar'
  })
  useCustomAvatar: boolean | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_followers'
  })
  numFollowers: number | null

  @Column({
    type: 'int',
    nullable: false,
    default: '0',
    name: 'num_following'
  })
  numFollowing: number

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_stars'
  })
  numStars: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_repos'
  })
  numRepos: number | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'description'
  })
  description: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_teams'
  })
  numTeams: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_members'
  })
  numMembers: number | null
}

import {
  Column,
  Entity,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('webhook',{ schema: 'chix_gogs' })
export class Webhook {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'org_id'
  })
  orgId: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'url'
  })
  url: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'content_type'
  })
  contentType: number | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'secret'
  })
  secret: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'events'
  })
  events: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_ssl'
  })
  isSsl: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_active'
  })
  isActive: boolean | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'hook_task_type'
  })
  hookTaskType: number | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'meta'
  })
  meta: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'last_status'
  })
  lastStatus: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix'
  })
  updated: string | null
}

import {
  Column,
  Entity,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('release',{ schema: 'chix_gogs' })
export class Release {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'publisher_id'
  })
  publisherId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'tag_name'
  })
  tagName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'lower_tag_name'
  })
  lowerTagName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'target'
  })
  target: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'title'
  })
  title: string | null

  @Column('varchar',{
    nullable: true,
    length: 40,
    name: 'sha1'
  })
  sha1: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'num_commits'
  })
  numCommits: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'note'
  })
  note: string | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '0',
    name: 'is_draft'
  })
  isDraft: boolean

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_prerelease'
  })
  isPrerelease: boolean | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null
}

import {
  Column,
  Entity,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('mirror',{ schema: 'chix_gogs' })
export class Mirror {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'interval'
  })
  interval: number | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '1',
    name: 'enable_prune'
  })
  enablePrune: boolean

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix'
  })
  updated: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'next_update_unix'
  })
  nextUpdateUnix: string | null
}

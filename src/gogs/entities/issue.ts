import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('issue',{ schema: 'chix_gogs' })
@Index('UQE_issue_repo_index',['repoId','index'],{ unique: true })
@Index('IDX_issue_repo_id',['repoId'])
export class Issue {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'index'
  })
  index: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'poster_id'
  })
  posterId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name'
  })
  name: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'content'
  })
  content: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'milestone_id'
  })
  milestoneId: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'priority'
  })
  priority: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'assignee_id'
  })
  assigneeId: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_closed'
  })
  isClosed: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_pull'
  })
  isPull: boolean | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_comments'
  })
  numComments: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'deadline_unix'
  })
  deadlineUnix: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix'
  })
  updated: string | null
}

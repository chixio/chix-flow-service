import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('milestone',{ schema: 'chix_gogs' })
@Index('IDX_milestone_repo_id',['repoId'])
export class Milestone {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name'
  })
  name: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'content'
  })
  content: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_closed'
  })
  Cislosed: boolean | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_issues'
  })
  numIssues: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_closed_issues'
  })
  numClosedIssues: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'completeness'
  })
  completeness: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'deadline_unix'
  })
  deadlineUnix: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'closed_date_unix'
  })
  closedDateUnix: string | null

}

import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('two_factor',{ schema: 'chix_gogs' })
@Index('UQE_two_factor_user_id',['userId'],{ unique: true })
export class TwoFactor {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    unique: true,
    name: 'user_id'
  })
  userId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'secret'
  })
  secret: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null
}

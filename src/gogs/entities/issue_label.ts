import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('issue_label',{ schema: 'chix_gogs' })
@Index('UQE_issue_label_s',['issueId','labelId'],{ unique: true })
export class IssueLabel {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'issue_id'
  })
  issueId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'label_id'
  })
  labelId: string | null
}

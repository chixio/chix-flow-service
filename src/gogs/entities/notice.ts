import {
  Column,
  Entity,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('notice',{ schema: 'chix_gogs' })
export class Notice {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'int',
    nullable: true,
    name: 'type'
  })
  type: number | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'description'
  })
  description: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null

}

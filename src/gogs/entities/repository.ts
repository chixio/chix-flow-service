import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('repository',{ schema: 'chix_gogs' })
@Index('UQE_repository_s',['ownerId','lowerName'],{ unique: true })
@Index('IDX_repository_lower_name',['lowerName'])
@Index('IDX_repository_name',['name'])
export class Repository {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'owner_id'
  })
  ownerId: string | null

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'lower_name'
  })
  lowerName: string

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'name'
  })
  name: string

  @Column({
    type: 'varchar',
    nullable: true,
    length: 512,
    name: 'description'
  })
  description: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'website'
  })
  website: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'default_branch'
  })
  defaultBranch: string | null

  @Column({
    type: 'bigint',
    nullable: false,
    default: '0',
    name: 'size'
  })
  size: string

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'use_custom_avatar'
  })
  useCustomAvatar: boolean | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_watches'
  })
  numWatches: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_stars'
  })
  numStars: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_forks'
  })
  numForks: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_issues'
  })
  numIssues: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_closed_issues'
  })
  numClosedIssues: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_pulls'
  })
  numPulls: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_closed_pulls'
  })
  numClosedPulls: number | null

  @Column({
    type: 'int',
    nullable: false,
    default: '0',
    name: 'num_milestones'
  })
  numMilestones: number

  @Column({
    type: 'int',
    nullable: false,
    default: '0',
    name: 'num_closed_milestones'
  })
  numClosedMilestones: number

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_private'
  })
  isPrivate: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_bare'
  })
  isBare: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_mirror'
  })
  isMirror: boolean | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '1',
    name: 'enable_wiki'
  })
  enableWiki: boolean

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'allow_public_wiki'
  })
  allowPublicWiki: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'enable_external_wiki'
  })
  enableExternalWiki: boolean | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'external_wiki_url'
  })
  externalWikiUrl: string | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '1',
    name: 'enable_issues'
  })
  enableIssues: boolean

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'allow_public_issues'
  })
  allowPublicIssues: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'enable_external_tracker'
  })
  enableExternalTracker: boolean | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'external_tracker_url'
  })
  externalTrackerUrl: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'external_tracker_format'
  })
  externalTrackerFormat: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'external_tracker_style'
  })
  externalTrackerStyle: string | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '1',
    name: 'enable_pulls'
  })
  enablePulls: boolean

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '0',
    name: 'pulls_ignore_whitespace'
  })
  pullsIgnoreWhitespace: boolean

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '0',
    name: 'pulls_allow_rebase'
  })
  pullsAllowRebase: boolean

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '0',
    name: 'is_fork'
  })
  isFork: boolean

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'fork_id'
  })
  forkId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix'
  })
  updated: string | null

}

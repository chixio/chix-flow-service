import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('action',{ schema: 'chix_gogs' })
@Index('IDX_action_repo_id',['repoId'])
export class Action {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'user_id'
  })
  userId: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'op_type'
  })
  opType: number | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'act_user_id'
  })
  actUserId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'act_user_name'
  })
  actUserName: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'repo_user_name'
  })
  repoUserName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'repo_name'
  })
  repoName: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'ref_name'
  })
  refName: string | null

  @Column({
    type: 'tinyint',
    nullable: false,
    width: 1,
    default: '0',
    name: 'is_private'
  })
  isPrivate: boolean

  @Column({
    type: 'text',
    nullable: true,
    name: 'content'
  })
  content: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null

}

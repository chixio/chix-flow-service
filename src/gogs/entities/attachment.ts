import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('attachment',{ schema: 'chix_gogs' })
@Index('UQE_attachment_uuid',['uuid'],{ unique: true })
@Index('IDX_attachment_issue_id',['issueId'])
@Index('IDX_attachment_release_id',['releaseId'])
export class Attachment {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'varchar',
    nullable: true,
    unique: true,
    length: 40,
    name: 'uuid'
  })
  uuid: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'issue_id'
  })
  issueId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'comment_id'
  })
  commentId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'release_id'
  })
  releaseId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name'
  })
  name: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null

}

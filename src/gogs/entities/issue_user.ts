import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('issue_user',{ schema: 'chix_gogs' })
@Index('IDX_issue_user_uid',['uid'])
@Index('IDX_issue_user_repo_id',['repoId'])
export class IssueUser {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'uid'
  })
  uid: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'issue_id'
  })
  issueId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'milestone_id'
  })
  milestoneId: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_read'
  })
  isRead: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_assigned'
  })
  isAssigned: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_mentioned'
  })
  isMentioned: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_poster'
  })
  isPoster: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_closed'
  })
  isClosed: boolean | null
}

import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('label',{ schema: 'chix_gogs' })
@Index('IDX_label_repo_id',['repoId'])
export class Label {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'name'
  })
  name: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 7,
    name: 'color'
  })
  color: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_issues'
  })
  numIssues: number | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'num_closed_issues'
  })
  numClosedIssues: number | null
}

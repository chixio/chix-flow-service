import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('access',{ schema: 'chix_gogs' })
@Index('UQE_access_s',['userId','repoId'],{ unique: true })
export class Access {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'user_id'
  })
  userId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'mode'
  })
  mode: number | null
}

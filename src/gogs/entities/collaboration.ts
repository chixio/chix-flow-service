import {
  Index,
  Entity,
  Column,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('collaboration',{ schema: 'chix_gogs' })
@Index('UQE_collaboration_s',['repoId','userId'],{ unique: true })
@Index('IDX_collaboration_repo_id',['repoId'])
@Index('IDX_collaboration_user_id',['userId'])
export class Collaboration {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: false,
    name: 'repo_id'
  })
  repoId: string

  @Column({
    type: 'bigint',
    nullable: false,
    name: 'user_id'
  })
  userId: string

  @Column({
    type: 'int',
    nullable: false,
    default: '2',
    name: 'mode'
  })
  mode: number
}

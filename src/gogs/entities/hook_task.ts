import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm'

@Entity('hook_task',{ schema: 'chix_gogs' })
@Index('IDX_hook_task_repo_id',['repoId'])
export class HookTask {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'repo_id'
  })
  repoId: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'hook_id'
  })
  hookId: string | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'uuid'
  })
  uuid: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'type'
  })
  type: number | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'url'
  })
  url: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'signature'
  })
  signature: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'payload_content'
  })
  payloadContent: string | null

  @Column({
    type: 'int',
    nullable: true,
    name: 'content_type'
  })
  contentType: number | null

  @Column({
    type: 'varchar',
    nullable: true,
    length: 255,
    name: 'event_type'
  })
  eventType: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_ssl'
  })
  isSsl: boolean | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_delivered'
  })
  isDelivered: boolean | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'delivered'
  })
  delivered: string | null

  @Column({
    type: 'tinyint',
    nullable: true,
    width: 1,
    name: 'is_succeed'
  })
  isSucceed: boolean | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'request_content'
  })
  requestContent: string | null

  @Column({
    type: 'text',
    nullable: true,
    name: 'response_content'
  })
  responseContent: string | null
}

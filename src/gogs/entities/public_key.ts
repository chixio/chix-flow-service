import {
  Column,
  Entity,
  Index, PrimaryGeneratedColumn
} from 'typeorm'

@Entity('public_key',{ schema: 'chix_gogs' })
@Index('IDX_public_key_owner_id',['ownerId'])
export class PublicKey {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id'
  })
  id: string

  @Column({
    type: 'bigint',
    nullable: false,
    name: 'owner_id'
  })
  ownerId: string

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'name'
  })
  name: string

  @Column({
    type: 'varchar',
    nullable: false,
    length: 255,
    name: 'fingerprint'
  })
  fingerprint: string

  @Column({
    type: 'text',
    nullable: false,
    name: 'content'
  })
  content: string

  @Column({
    type: 'int',
    nullable: false,
    default: '2',
    name: 'mode'
  })
  mode: number

  @Column({
    type: 'int',
    nullable: false,
    default: '1',
    name: 'type'
  })
  type: number

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'created_unix'
  })
  created: string | null

  @Column({
    type: 'bigint',
    nullable: true,
    name: 'updated_unix'
  })
  updated: string | null

}

import * as express from 'express'
import * as request from 'superagent'
import { ApplicationModule } from '../app.module'
import { Test } from '@nestjs/testing'
import { LogService, LoggerModule } from '@nestling/logger'
import { UserModule } from '../user/user.module'
import { GogsDbStrategy } from '../user/strategies/gogs-db.strategy'
import { DatabaseAdapterModule } from '../adapter'
import { MongoDbAdapter } from '../adapter/mongo.adapter'
import { ConfigModule, ConfigService } from '@nestling/config'
import { INestApplication, INestExpressApplication } from '@nestjs/common'
import { Express } from 'express'
import { generateFakeAccessToken } from '.'
import { Request } from 'superagent'
import { ensureTestUser } from './typeorm'
import { User } from '../gogs/entities/user.entity'
import { getRepository, getConnection } from 'typeorm'
import { connectDb } from './typeorm/connectDb'
import { getRepoManager } from '../git/getRepoManager'
import { ContextService } from '@nestling/context'
import { RepoManager } from '../git/RepoManager'

export type TestUser = User & {
  accessToken: string
  authorizationHeader: string
}

export interface TestContext {
  app: INestApplication & INestExpressApplication
  config: any
  server: Express
  user: TestUser
  user2: TestUser
  agent: Request
  agent2: Request
  repoManager: RepoManager
  mongoAdapter: MongoDbAdapter
  close: () => Promise<void>
}

export async function createTestApp (collection: string): Promise<TestContext> {
  await connectDb()
  await ensureTestUser()

  const module = await Test.createTestingModule({
    imports: [ApplicationModule]
  })
    .compile()

  const server = express()
  const app = module.createNestApplication(server)

  const logger = await app
    .get(LogService)

  logger.level('info')

  await app.init()

  logger.info('Test app initialized')

  const gogsDbStrategy = await app
    .select(UserModule)
    .get(GogsDbStrategy)

  await gogsDbStrategy.start()

  logger.info('Started gogs db strategy')

  logger.info('Flushing mongo db')
  const mongoAdapter = await app
    .select(DatabaseAdapterModule)
    .get(MongoDbAdapter)

  await mongoAdapter.flush(collection)

  const config = await app
    .select(ConfigModule)
    .get(ConfigService) as any

  const context = await app
    .get(ContextService)

  context.set('logger', logger)

  logger.info('Test context initialized.')

  const userRepository = getRepository(User)

  // let's do this with gogsClient btw.
  const testUser = await userRepository.findOne({ where: { name: 'test-user' } }) as TestUser
  const testUser2 = await userRepository.findOne({ where: { name: 'test-user2' } }) as TestUser

  // These can now be the real tokens, not the jwt ones.
  testUser.accessToken = await generateFakeAccessToken(
    testUser.id,
    testUser.name,
    config.jwt.secret
  )
  testUser.authorizationHeader = `Bearer ${testUser.accessToken}`

  testUser2.accessToken = await generateFakeAccessToken(
    testUser2.id,
    testUser2.name,
    config.jwt.secret
  )
  testUser2.authorizationHeader = `Bearer ${testUser2.accessToken}`

  const agent = (request.agent() as any)
    .set('Authorization', `Bearer ${testUser.accessToken}`)

  const agent2 = (request.agent() as any)
    .set('Authorization', `Bearer ${testUser2.accessToken}`)

  logger.info('Test users initialized.')

  context.set('Authorization', testUser.authorizationHeader)

  const repoManager = getRepoManager(context, testUser.name)

  logger.info(`Clearing repositories for user ${testUser.name}`)
  await repoManager.cleanCloneDir()

  await repoManager.clearRepos(testUser.name)

  logger.info('Create testApp done.')

  const close = async () => {
    getConnection().close()
    await mongoAdapter.close()
  }

  return {
    app,
    config,
    server,
    user: testUser,
    user2: testUser2,
    agent,
    agent2,
    repoManager,
    mongoAdapter,
    close
  }
}

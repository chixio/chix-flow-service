import { createConnection } from 'typeorm'
import { User } from '../../gogs/entities/user.entity'
import { environment } from '../../environment/environment'
import * as objenv from 'objenv'

let connection
export async function connectDb () {
  const config = objenv(environment).mysql
  const {
    host,
    port,
    username,
    password,
    database,
    logging
  } = config as any

  const connectionOptions = {
    type: 'mysql',
    host,
    port,
    username,
    password,
    database,
    entities: [
      User
    ],
    bigNumberStrings: false,
    logging
  }

  if (!connection) {
    connection = await createConnection(connectionOptions as any)
  }
}

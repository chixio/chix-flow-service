import { getRepository } from 'typeorm'
import { User } from '../../gogs/entities/user.entity'
import { GogsClient } from '../../git'

const USER_TEST_NAME = 'test-user'
const ADMIN_TEST_NAME = 'test-admin'

export const ensureTestUser = async () => {
  const repo = getRepository(User)

  let found = repo.find({ where: { name: USER_TEST_NAME } })

  if (!found) {
    console.log('Test user not found, create one first')

    process.exit()
  }

  found = repo.find({ where: { name: ADMIN_TEST_NAME } })

  if (!found) {
    console.log('Test admin user not found, create one first')

    process.exit()
  }

  return found
}

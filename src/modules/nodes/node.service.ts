import { Collection } from 'mongodb'

import {
  NodeDefinition,
  NodeModel,
  UserModel
} from '@chix/common'

import { Injectable } from '@nestjs/common'

import { ContextModel } from '@nestling/context'
import { ErrorMessage } from '@nestling/errors'
import { LogService } from '@nestling/logger'
import { ValidatorService } from '@nestling/validator'

import { MongoDbAdapter } from '../../adapter/mongo.adapter'
import { getIdentifier } from './utils'
import { Pagination } from '../../paginate'
import { UserService } from '../../user/user.service'
import { RepoManager } from '../../git/RepoManager'

export interface Scope {
  user?: string
}

export interface RepoManagerContext {
  repoManager: RepoManager
}

@Injectable()
export class NodeService {
  constructor (
    private log: LogService,
    private adapter: MongoDbAdapter,
    private validatorService: ValidatorService,
    private userService: UserService
  ) {
    this.adapter.addDocFilterFor('nodes', this.UserFilter.bind(this))
  }

  private async UserFilter<T> (document): Promise<T> {
    if (document.providerId) {
      const user = await this.userService.getUser(document.providerId)

      delete document.providerId

      return {
        ...document as any,
        // TODO: make sure this does not become ambigious
        provider: {
          ...user
        }
      }
    }

    return document
  }

  /**
   * Ensures the text search index exists.
   *
   * Used to do text search during the search operation.
   *
   * @returns {Promise<void>}
   */
  async ensureIndex () {
    const collection: Collection = await this.adapter.db.collection('nodes')

    try {
      await collection.createIndex({
        ns: 'text',
        name: 'text',
        description: 'text'
      }, {
        name: 'text_search_index'
      })

      this.log.info('Created text_search_index.')
    } catch (e) {}
  }

  async getByNamespaceAndName (
    ns: string,
    name: string,
    scope: Scope = {}
  ): Promise<NodeModel> {
    return this.adapter.findOne<NodeModel>(
      'nodes',
      {
        ns,
        name,
        ...scope
      })
  }

  async getByUserNamespaceAndName (
    user: UserModel,
    ns: string,
    name: string,
    scope: Scope = {}
    ): Promise<NodeModel> {
    return this.adapter.findOne<NodeModel>(
      'nodes',
      {
        providerId: user.id,
        ns,
        name,
        ...scope
      })
  }

  async getById (id: string): Promise<NodeModel> {
    return this.adapter.findOne<NodeModel>('nodes',{ id })
  }

  async getByUserAndId (
    user: UserModel,
    id: string
  ): Promise<NodeModel> {
    const node = await this.adapter.findOne<NodeModel>(
      'nodes',
      {
        providerId: user.id,
        id
      }
    )

    if (!node) {
      throw new ErrorMessage('node:notFound')
    }

    if (node._id) delete node._id

    return node
  }

  async find (
    context: ContextModel,
    where = {},
    options?: Pagination
  ): Promise<NodeModel[]> {
    return this.adapter.find<NodeModel>(
      'nodes',
      where,
      options,
      context
    )
  }

  async search (
    context: ContextModel,
    term: string,
    options?: Pagination,
    where: any = {}
  ): Promise<NodeModel[]> {
    return this.adapter.search<NodeModel>(
      context,
      'nodes',
      term,
      options,
      where
    )
  }

  async findByNamespace (
    context: ContextModel,
    ns: string,
    options?: Pagination
  ): Promise<NodeModel[]> {
    return this.adapter.find<NodeModel>(
      'nodes',
      {
        ns
      },
      options,
      context
    )
  }

  async findByUser (
    context: ContextModel,
    user: UserModel,
    where: any,
    options?: Pagination
  ): Promise<NodeModel[]> {
    return this.adapter.find<NodeModel>(
      'nodes',
      {
        providerId: user.id,
        ...where
      },
      options,
      context
    )
  }

  async findByUserNamespace (
    context: ContextModel,
    user: UserModel,
    ns: string,
    where: any = {},
    options?: Pagination
  ): Promise<NodeModel[]> {
    return this.adapter.find<NodeModel>(
      'nodes',{
        providerId: user.id,
        ns,
        ...where
      },
      options,
      context
    )
  }

  async findByNamespaceAndName (
    ns: string,
    name: string,
    where: any = {}
  ): Promise<NodeModel> {
    return this.adapter.findOne<NodeModel>(
      'nodes',
      {
        ns,
        name,
        ...where
      }
    )
  }

  async findByUserNamespaceAndName (
    user: UserModel,
    ns: string,
    name: string,
    where: any = {}
  ): Promise<NodeModel> {
    return this.adapter.findOne<NodeModel>(
      'nodes',{
        providerId: user.id,
        ns,
        name,
        ...where
      }
    )
  }

  async create (
    user: UserModel,
    node: NodeDefinition,
    { repoManager }: RepoManagerContext
  ): Promise<NodeModel> {
    await this.validatorService.validate('node', node)

    const found = await this.getByUserNamespaceAndName(user, node.ns, node.name)

    if (found) {
      throw new ErrorMessage('node:alreadyExists', {
        ns: node.ns,
        name: node.name
      })
    }

    (this.log as any).info(`Creating Node ${getIdentifier(node)}`)

    await repoManager.openRepository(node.ns)

    await repoManager.putNode(node)

    const result = await this.adapter.insert<NodeModel>('nodes', {
      providerId: user.id,
      ...node
    })

    ;(this.log as any).info(`Created node: ${getIdentifier(result)}`)

    if (node._id) delete node._id
    if (result._id) delete result._id

    return result
  }

  async createMany (
    user: UserModel,
    nodes: NodeDefinition[],
    context: RepoManagerContext
  ): Promise<NodeModel[]> {
    const promises = nodes.map((node: NodeDefinition) => this.create(user, node, context))

    return Promise.all<NodeModel>(promises)
  }

  async update (
    user: UserModel,
    node: NodeModel,
    { repoManager }: RepoManagerContext
  ): Promise<NodeModel> {
    (this.log as any).info(`saving Node ${node.ns}:${node.name}`)

    await this.validatorService.validate('node', node)

    let found

    await repoManager.openRepository(node.ns)

    if (node.id) {
      found = await this.getByUserAndId(user, node.id)

      if (!found) {
        throw new ErrorMessage('node:notFound')
      }

      if (node.name !== found.name) {
        this.log.info(`Removing node ${found.ns}:${found.name} as it will be renamed.`)
        await repoManager.removeNode(found.name)
      }
    } else {
      found = await this.getByUserNamespaceAndName(user, node.ns, node.name)

      if (!found) {
        node.providerId = user.id
      }
    }

    const bareNode = Object.keys(node).reduce((_node, key) => {
      if (key !== 'providerId') {
        _node[key] = node[key]
      }

      return _node
    }, {} as NodeDefinition)

    await repoManager.putNode(bareNode)

    const result = await this.adapter.update<NodeModel>('nodes', {
      ...node,
      id: found ? found.id : undefined
    })

    ;(this.log as any).info(`Saved node ${getIdentifier(node)}`)

    return result
  }

  async renameNs (
    user: UserModel,
    oldNs: string,
    newNs: string,
    context: RepoManagerContext
  ): Promise<void> {
    (this.log as any).info(`renaming node namespace ${oldNs} -> ${newNs}`)

    const col = this.adapter.db.collection('nodes')

    const result = await col.update({
      ns: oldNs,
      providerId: user.id
    }, {
      $set: { ns: newNs }
    }, {
      multi: true
    })

    const nodes = await col.find({ ns: newNs }).toArray()

    if (nodes.length) {
      this.log.info(`Updating ${nodes.length} renamed nodes.`)
      await this.updateMany(user, nodes, context)
    } else {
      this.log.info(`renameNs: found no ${newNs} nodes to update.`)
    }
  }

  async updateMany (
    user: UserModel,
    nodes: NodeModel[],
    context: RepoManagerContext
  ): Promise<NodeModel[]> {
    const promises = nodes.map((node) => this.update(user, node, context))

    return Promise.all<NodeModel>(promises)
  }

  async remove (
    id: string,
    { repoManager }: RepoManagerContext
  ) {
    (this.log as any).info(`Removing Node: ${id}`)

    const node = await this.adapter.findById<NodeModel>('nodes', id)

    if (node) {
      await repoManager.removeNode(node.name)

      return this.adapter.remove('nodes', id)
    }

    return false
  }

  async exists (user: UserModel, where: any) {
    (this.log as any).info(`Test if exists: ${Object.keys(where)}`)

    return this.adapter.exists('nodes', {
      providerId: user.id,
      ...where
    })
  }

  async removeByNamespaceAndName (
    ns: string,
    name: string,
    { repoManager }: RepoManagerContext
  ): Promise<boolean> {
    (this.log as any).info(`Removing Node by namespace and name: ${ns}:${name}`)

    const node = await this.adapter.findOne<NodeModel>(
      'nodes',
      {
        ns,
        name
      }
    )

    if (node) {
      await repoManager.openRepository(node.ns)

      await repoManager.removeNode(node.name)

      await repoManager.closeRepository()

      return this.adapter.remove('nodes', node.id)
    }

    return false
  }

  async removeByUserNamespace (
    user: UserModel,
    ns: string,
    { repoManager }: RepoManagerContext
  ): Promise<boolean> {
    (this.log as any).info(`Removing Nodes by namespace: ${ns}`)

    const nodes = await this.adapter.find<NodeModel>('nodes', {
      providerId: user.id,
      ns
    })

    if (nodes.length) {
      const names = nodes.map((node) => node.name)

      try {
        await repoManager.openRepository(ns)
        await repoManager.removeNodes(names)
      } catch (error) {
        this.log.warn(`Failed to remove (some) nodes from repository continuing anyway.`)
      } finally {
        await repoManager.closeRepository()
      }

      return this.adapter.removeAll('nodes', {
        ns,
        providerId: user.id
      })
    }

    return false
  }

  async removeByUserNamespaceAndName (
    user: UserModel,
    ns: string,
    name: string,
    { repoManager }: RepoManagerContext
  ): Promise<boolean> {
    (this.log as any).info(`Removing Node by namespace and name: ${ns}:${name}`)

    const node = await this.adapter.findOne<NodeModel>('nodes', {
      providerId: user.id,
      ns,
      name
    })

    if (node) {
      await repoManager.removeNode(node.name)

      return this.adapter.remove('nodes', node.id)
    }

    return false
  }

  async flush () {
    return this.adapter.flush('nodes')
  }

  async close () {
    return this.adapter.close()
  }
}

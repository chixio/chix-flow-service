import { assert, expect } from 'chai'
import { Test } from '@nestjs/testing'
import { TestingModule } from '@nestjs/testing/testing-module'
import { ContextModel } from '@nestling/context'
import { LogService } from '@nestling/logger'
import { UserModel } from '@chix/common'

import { NodeService } from './index'
import { node } from './__mocks__/node'
import { TestContext, createTestApp } from '../../support/createTestApp'
import { ProjectService } from '../projects'
import { RepoManager } from '../../git/RepoManager'

const context = new ContextModel()

describe('Node Service', () => {
  let module: TestingModule
  let nodeService: NodeService
  let testApp: TestContext
  let repoManager: RepoManager
  let projectService: ProjectService

  before(async () => {
    testApp = await createTestApp('nodes')

    const logger = testApp.app.get(LogService)

    logger.info('Test App created')

    projectService = testApp.app.get(ProjectService)

    repoManager = testApp.repoManager

    // Flushes *all* projects.
    await projectService.flush({
      repoManager
    }, {})

    await projectService.create(testApp.user, {
      name: 'console',
      description: 'console test project'
    }, {
      repoManager: testApp.repoManager
    })

    nodeService = testApp.app.get(NodeService)

    await nodeService.flush()
  })

  after(async () => {
    await projectService.flush({
      repoManager: testApp.repoManager
    }, {})

    if (nodeService) {
      nodeService.close()
    }

    await testApp.close()
  })

  it('ensures text index', async () => {
    await nodeService.ensureIndex()
  })

  it('creates new node', async () => {
    const _node = await nodeService.create(testApp.user, node, {
      repoManager
    })

    assert.isOk(_node.id)
    expect(_node.provider).to.eql({
      id: testApp.user.id,
      name: testApp.user.name
    })

    delete _node.id
    delete _node.providerId

    expect(_node).to.deep.equal({
      ...node,
      provider: {
        id: testApp.user.id,
        name: testApp.user.name
      }
    })

    await repoManager.closeRepository()
  })

  it('finds all nodes', async () => {
    const nodes = await nodeService.find(context)

    assert.isOk(nodes.length === 1)
    assert.isOk(nodes[0].id)
  })

  it('can find node as part of ns', async () => {
    const nodes = await nodeService.search(context,'console')

    assert.isOk(nodes.length === 1)
    assert.isOk(nodes[0].id)
  })

  it('can find node as part of name', async () => {
    const nodes = await nodeService.search(context,'log')

    assert.isOk(nodes.length === 1)
    assert.isOk(nodes[0].id)
  })

  it('can find node as part of description', async () => {
    const nodes = await nodeService.search(context,'logger')

    assert.isOk(nodes.length === 1)
    assert.isOk(nodes[0].id)
  })

  it('finds node by namespace', async () => {
    const nodes = await nodeService.findByNamespace(
      context,
      'console'
    )

    assert.isOk(nodes.length === 1)
    assert.isOk(nodes[0].id)
  })

  it('finds node by user and namespace', async () => {
    const nodes = await nodeService.findByUserNamespace(
      context,
      testApp.user,
      'console'
    )

    assert.isOk(nodes.length === 1)
    assert.isOk(nodes[0].id)
  })

  it('finds node by namespace and name', async () => {
    const node = await nodeService.findByNamespaceAndName('console', 'log')

    assert.isOk(node.id)
  })

  it('finds node by user, namespace and name', async () => {
    const node = await nodeService.findByUserNamespaceAndName(
      testApp.user,
      'console',
      'log'
    )

    assert.isOk(node.id)
  })

  it('updates node', async () => {
    const node = await nodeService.findByUserNamespaceAndName(
      testApp.user,
      'console',
      'log'
    )

    const updated = await nodeService.update(
      testApp.user, {
        ...node,
        name: 'log2'
      }, {
        repoManager: testApp.repoManager
      })

    expect(updated.ns).to.equal('console')
    expect(updated.name).to.equal('log2')
  })

  it('removes node user, namespace and name', async () => {
    const node = await nodeService.findByUserNamespaceAndName(
      testApp.user,
      'console',
      'log2'
    )

    const removed = await nodeService.removeByUserNamespaceAndName(
      testApp.user,
      node.ns,
      node.name, {
        repoManager: testApp.repoManager
      })

    expect(removed).to.equal(true)
  })
})

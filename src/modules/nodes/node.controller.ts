import { NodeDefinition } from '@chix/common'
import {
  Body,
  Controller,
  Get,
  Delete,
  HttpStatus,
  HttpCode,
  Param,
  Query,
  Post,
  Put,
  OnModuleInit, Head, Res
} from '@nestjs/common'
import { NodeService } from './node.service'
import { User } from '../../user'
import { Page, Pagination } from '../../paginate'
import { Context } from '@nestling/context'
import { publicOrOwnFilter } from '../shared'
import { UserService } from '../../user/user.service'
import { ErrorMessage } from '@nestling/errors'
import { getRepoManager } from '../../git/getRepoManager'
import { SharedService } from '../../shared/shared.service'

@Controller('nodes')
export class NodeController implements OnModuleInit {
  constructor (
    private readonly nodeService: NodeService,
    private readonly userService: UserService,
    private readonly sharedService: SharedService
  ) {}

  async onModuleInit () {
    await this.nodeService.ensureIndex()
  }

  @Get('')
  @HttpCode(HttpStatus.OK)
  async find (
    @Context() context,
    @User() user,
    @Page() page: Pagination
  ): Promise<NodeDefinition[]> {
    const $or = publicOrOwnFilter(user)

    return this.nodeService.find(
      context,
      { $or },
      { ...page })
  }

  @Get('search')
  @HttpCode(HttpStatus.OK)
  async search (
    @Context() context,
    @User() user,
    @Query('q') q,
    @Page() page: Pagination
  ): Promise<NodeDefinition[]> {
    const $or = publicOrOwnFilter(user)

    return this.nodeService.search(
      context,
      q,
      { ...page },
      { $or }
    )
  }

  // TODO: clashes a bit with /search
  @Get(':provider')
  @HttpCode(HttpStatus.OK)
  async findByUser (
    @Context() context,
    @User() user,
    @Param('provider') providerName,
    @Page() page: Pagination
  ): Promise<NodeDefinition[]> {
    if (user && user.name === providerName) {
      return this.nodeService.findByUser(
        context,
        user,
        {},
        { ...page }
      )
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.nodeService.findByUser(
      context,
      provider,
      {
        private: {
          $ne: true
        }
      },
      {
        ...page
      }
    )
  }

  @Head(':provider/:ns')
  @HttpCode(HttpStatus.OK)
  async userNamespaceExists (
    @User() user,
    @Param('provider') providerName,
    @Param('ns') ns,
    @Res() response
  ) {
    let exists

    if (user && user.name === providerName) {
      exists = await this.nodeService.exists(
        user,
        { ns }
      )
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.nodeService.exists(
        provider,
        {
          ns,
          private: {
            $ne: true
          }
        }
      )
    }

    if (exists) {
      return response.status(HttpStatus.OK)
    }

    return response.status(HttpStatus.NOT_FOUND)
  }

  @Get(':provider/:ns')
  @HttpCode(HttpStatus.OK)
  async findByUserNamespace (
    @Context() context,
    @User() user,
    @Param('provider') providerName,
    @Param('ns') ns,
    @Page() page: Pagination
  ): Promise<NodeDefinition[]> {
    if (user && user.name === providerName) {
      return this.nodeService.findByUserNamespace(
        context,
        user,
        ns,
        {},
        {
          ...page
        }
      )
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.nodeService.findByUserNamespace(
      context,
      provider,
      ns,
      {
        private: {
          $ne: true
        }

      },
      { ...page }
    )
  }

  @Head(':provider/:ns/:name')
  async userNamespaceAndNameExists (
    @User() user,
    @Param('provider') providerName,
    @Param('ns') ns,
    @Param('name') name,
    @Res() response
  ) {
    let exists

    if (user && user.name === providerName) {
      exists = this.nodeService.exists(
        user,
        { ns, name }
      )
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.nodeService.exists(
        provider,
        {
          ns,
          name,
          private: {
            $ne: true
          }
        }
      )
    }

    if (exists) {
      return response.status(HttpStatus.OK)
    }

    return response.status(HttpStatus.NOT_FOUND)
  }

  @Get(':provider/:ns/:name')
  @HttpCode(HttpStatus.OK)
  async findByUserNamespaceAndName (
    @User() user,
    @Param('provider') providerName,
    @Param('ns') ns,
    @Param('name') name
  ): Promise<NodeDefinition> {
    if (user && user.name === providerName) {
      return this.nodeService.findByUserNamespaceAndName(user, ns, name)
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.nodeService.findByUserNamespaceAndName(
      provider,
      ns,
      name,
      {
        private: {
          $ne: true
        }
      }
    )
  }

  @Post(':provider')
  @HttpCode(HttpStatus.CREATED)
  async create (
    @User({ required: true }) user,
    @Context() context,
    @Param('provider') provider,
    @Body() node
  ) {
    const projectExists = await this.sharedService.projectExists(user, {
      name: node.ns
    })

    if (projectExists) {
      if (user && user.name === provider) {
        const repoManager = getRepoManager(context, provider)

        await repoManager.openRepository(node.ns)

        let result

        if (Array.isArray(node)) {
          result = await this.nodeService.createMany(user, node, {
            repoManager
          })
        } else {
          result = await this.nodeService.create(user, node, {
            repoManager
          })
        }

        await repoManager.closeRepository()

        return result
      }

      throw new ErrorMessage('user:invalid')
    }

    throw new ErrorMessage('node:projectNotFound', { provider, name: node.ns })
  }

  @Put(':provider')
  @HttpCode(HttpStatus.OK)
  async update (
    @User() user,
    @Context() context,
    @Param('provider') provider,
    @Body() node
  ) {
    const projectExists = await this.sharedService.projectExists(user, {
      name: node.ns
    })

    if (projectExists) {
      if (user && user.name === provider) {
        const repoManager = getRepoManager(context, provider)

        await repoManager.openRepository(node.ns)

        let result

        if (Array.isArray(node)) {
          result = await this.nodeService.updateMany(user, node, {
            repoManager
          })
        } else {
          result = await this.nodeService.update(user, node, {
            repoManager
          })
        }

        await repoManager.closeRepository()

        return result
      }

      throw new ErrorMessage('user:invalid')
    }

    throw new ErrorMessage('node:projectNotFound', { provider, name: node.ns })
  }

  @Delete(':provider/:ns/:name')
  @HttpCode(HttpStatus.NO_CONTENT)
  async removeByUserNamespaceAndName (
    @User() user,
    @Context() context,
    @Param('provider') provider,
    @Param('ns') ns,
    @Param('name') name
  ) {
    if (user && user.name === provider) {
      const repoManager = getRepoManager(context, provider)

      await repoManager.openRepository(ns)

      await this.nodeService.removeByUserNamespaceAndName(user, ns, name, {
        repoManager
      })

      await repoManager.closeRepository()
    } else {
      throw new ErrorMessage('user:invalid')
    }
  }
}

export function getIdentifier (node) {
  return `${node.ns}:${node.name}`
}

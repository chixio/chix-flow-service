import { Module } from '@nestjs/common'
import { NodeController } from './node.controller'
import { NodeService } from './node.service'
import { nodeErrors } from './node.errors'
import { ErrorMessage } from '@nestling/errors'
import { SharedModule } from '../../shared/shared.module'
import { DatabaseAdapterModule } from '../../adapter'
import { ContextModule } from '@nestling/context'
import { LoggerModule } from '@nestling/logger'
import { ValidatorModule } from '@nestling/validator'
import { UserModule } from '../../user/user.module'

ErrorMessage.addErrorMessages(nodeErrors)

@Module({
  imports: [
    SharedModule,
    LoggerModule,
    ValidatorModule,
    DatabaseAdapterModule,
    UserModule,
    ContextModule
  ],
  controllers: [
    NodeController
  ],
  providers: [
    NodeService
  ],
  exports: [
    NodeService
  ]
})
export class NodeModule {}

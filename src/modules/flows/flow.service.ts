import { Collection } from 'mongodb'
import * as uuid from 'uuid'

import {
  Flow as FlowDefinition,
  FlowModel,
  UserModel
} from '@chix/common'

import { Injectable } from '@nestjs/common'

import { ContextModel } from '@nestling/context'
import { ErrorMessage } from '@nestling/errors'
import { LogService } from '@nestling/logger'
import { ValidatorService } from '@nestling/validator'

import { MongoDbAdapter } from '../../adapter/mongo.adapter'
import { ensureUniqueIds } from './utils/ensureUniqueIds'
import { Pagination } from '../../paginate'
import { RepoManagerContext } from '../nodes'
import { UserService } from '../../user/user.service'

@Injectable()
export class FlowService {
  constructor (
    private log: LogService,
    private adapter: MongoDbAdapter,
    private validatorService: ValidatorService,
    private userService: UserService
  ) {
    this.adapter.addDocFilterFor('flows', this.UserFilter.bind(this))
  }

  private async UserFilter<T> (document): Promise<T> {
    if (document.providerId) {
      const user = await this.userService.getUser(document.providerId)

      delete document.providerId

      return {
        ...document as any,
        // TODO: make sure this does not become ambigious
        provider: {
          ...user
        }
      }
    }

    return document
  }

  /**
   * Ensures the text search index exists.
   *
   * Used to do text search during the search operation.
   *
   * @returns {Promise<void>}
   */
  async ensureIndex () {
    const collection: Collection = await this.adapter.db.collection('flows')

    try {
      await collection.createIndex({
        ns: 'text',
        name: 'text',
        description: 'text'
      }, {
        name: 'text_search_index'
      })

      this.log.info('Created text_search_index.')
    } catch (e) {}
  }

  async getByNamespaceAndName (
    ns: string,
    name: string
  ): Promise<FlowDefinition> {
    const flow = await this.adapter.findOne<FlowDefinition>(
      'flows',
      {
        ns,
        name
      }
    )

    if (!flow) {
      throw new ErrorMessage('flow:notFound')
    }

    if (flow._id) delete flow._id

    return flow
  }

  async getByUserNamespaceAndName (
    user: UserModel,
    ns: string,
    name: string
  ): Promise<FlowModel> {
    const flow = await this.adapter.findOne<FlowModel>(
      'flows',
      {
        providerId: user.id,
        ns,
        name
      }
    )

    if (flow && flow._id) delete flow._id

    return flow
  }

  async getById (id: string): Promise<FlowModel> {
    const flow = await this.adapter.findOne<FlowModel>(
      'flows',
      {
        id
      }
    )

    if (!flow) {
      throw new ErrorMessage('flow:notFound')
    }

    if (flow._id) delete flow._id

    return flow
  }

  async getByUserAndId (
    user: UserModel,
    id: string
  ): Promise<FlowModel> {
    const flow = await this.adapter.findOne<FlowModel>(
      'flows',
      {
        providerId: user.id,
        id
      }
    )

    if (!flow) {
      throw new ErrorMessage('flow:notFound')
    }

    if (flow._id) delete flow._id

    return flow
  }

  async find (
    context: ContextModel,
    where = {},
    options?: Pagination
  ): Promise<FlowModel[]> {
    return this.adapter.find<FlowModel>(
      'flows',
      where,
      options,
      context
    )
  }

  async search (
    context: ContextModel,
    term: string,
    options?: Pagination,
    where: any = {}
  ): Promise<FlowModel[]> {
    return this.adapter.search<FlowModel>(
      context,
      'flows',
      term,
      options,
      where
    )
  }

  async findByNamespace (
    context: ContextModel,
    ns: string,
    options?: Pagination
  ): Promise<FlowModel[]> {
    return this.adapter.find<FlowModel>(
      'flows',
      {
        ns
      },
      options,
      context
    )
  }

  async findByUser (
    context: ContextModel,
    user: UserModel,
    where: any = {},
    options?: Pagination
  ): Promise<FlowModel[]> {
    return this.adapter.find<FlowModel>(
      'flows',{
        providerId: user.id,
        ...where
      },
      options,
      context
    )
  }

  async findByUserNamespace (
    context: ContextModel,
    user: UserModel,
    ns: string,
    where: any = {},
    options?: Pagination
  ): Promise<FlowModel[]> {
    return this.adapter.find<FlowModel>(
      'flows', {
        providerId: user.id,
        ns,
        ...where
      },
      options,
      context
    )
  }

  async findByNamespaceAndName (
    ns: string,
    name: string,
    where: any = {}
  ): Promise<FlowModel> {
    const flow = this.adapter.findOne<FlowModel>(
      'flows',
      {
        ns,
        name,
        ...where
      }
    )

    if (!flow) {
      throw new ErrorMessage('flow:notFound')
    }

    return flow
  }

  async findByUserNamespaceAndName (
    user: UserModel,
    ns: string,
    name: string,
    where: any = {}
  ): Promise<FlowModel> {
    const flow = this.adapter.findOne<FlowModel>(
      'flows',
      {
        providerId: user.id,
        ns,
        name,
        ...where
      }
    )

    if (!flow) {
      throw new ErrorMessage('flow:notFound')
    }

    return flow
  }

  async create (
    user: UserModel,
    flow: FlowDefinition,
    { repoManager }: RepoManagerContext
  ): Promise<FlowModel> {
    (this.log as any).info(`Creating Flow ${flow.title}`)

    await this.validatorService.validate('flow', flow)

    const found = await this.getByUserNamespaceAndName(user, flow.ns, flow.name)

    if (found) {
      throw new ErrorMessage('flow:alreadyExists')
    }

    if ((flow as any)._isCopy) {
      flow.id = uuid.v4()
      ensureUniqueIds(flow, (this.log as any))
    } else if ((flow as any)._isNew || !flow.id) {
      flow.id = flow.id || uuid.v4()
    }

    delete (flow as any)._isNew
    delete (flow as any)._isCopy

    await repoManager.openRepository(flow.ns)

    await repoManager.putFlow(flow)

    const result = await this.adapter.insert<FlowModel>('flows', {
      providerId: user.id,
      ...flow
    })

    ;(this.log as any).info(`Created flow: ${result.title}`)

    if (flow._id) delete flow._id
    if (result._id) delete result._id

    return result
  }

  async createMany (
    user: UserModel,
    flows: FlowDefinition[],
    context: RepoManagerContext
  ): Promise<FlowModel[]> {
    const promises = flows.map((flow: FlowDefinition) => this.create(user, flow, context))

    return Promise.all<FlowModel>(promises)
  }

  async update (
    user: UserModel,
    flow: FlowModel,
    { repoManager }: RepoManagerContext
  ): Promise<FlowModel> {
    this.log.info(`saving Flow ${flow.ns}:${flow.name}`)

    await this.validatorService.validate('flow', flow)

    let found
  
    await repoManager.openRepository(flow.ns)

    if (flow.id) {
      found = await this.getByUserAndId(user, flow.id)

      if (!found) {
        throw new ErrorMessage('flow:notFound')
      }

      if (flow.name !== found.name) {
        this.log.info(`Removing flow ${found.ns}:${found.name} as it will be renamed.`)
        await repoManager.removeFlow(found.name)
      }
    } else {
      found = await this.getByUserNamespaceAndName(user, flow.ns, flow.name)

      if (!found) {
        flow.providerId = user.id
      }
    }

    await repoManager.putFlow({
      ...flow,
      providerId: undefined
    } as FlowDefinition)

    const result = await this.adapter.update<FlowModel>('flows', {
      ...flow,
      id: found ? found.id : undefined
    })

    this.log.info(`Saved flow ${flow.title}`)

    return result
  }

  async renameNs (
    user: UserModel,
    oldNs: string,
    newNs: string,
    context: RepoManagerContext
  ): Promise<void> {
    (this.log as any).info(`renaming flow namespace ${oldNs} -> ${newNs}`)

    const col = this.adapter.db.collection('flows')

    await col.update({
      ns: oldNs,
      providerId: user.id
    }, {
      $set: {
        ns: newNs
      }
    }, {
      multi: true
    })

    const flows = await col.find({ ns: newNs }).toArray()

    if (flows.length) {
      await this.updateMany(user, flows, context)
    }
  }

  async updateMany (
    user: UserModel,
    flows: FlowModel[],
    context: RepoManagerContext
  ): Promise<FlowModel[]> {
    const promises = flows.map((flow) => this.update(user, flow, context))

    return Promise.all<FlowModel>(promises)
  }

  async remove (
    id: string,
    { repoManager }: RepoManagerContext
  ): Promise<boolean> {
    (this.log as any).info('Removing Flow')

    const flow = await this.adapter.findById<FlowModel>('flows', id)

    if (flow) {
      await repoManager.removeFlow(flow.name)

      return this.adapter.remove('flows', id)
    }

    return false
  }

  async exists (user: UserModel, where: any) {
    (this.log as any).info(`Test if exists: ${Object.keys(where)}`)

    return this.adapter.exists('flows', {
      providerId: user.id,
      ...where
    })
  }

  async removeByNamespaceAndName (
    ns: string,
    name: string,
    { repoManager }: RepoManagerContext
  ): Promise<boolean> {
    (this.log as any).info(`Removing Flow by namespace and name: ${ns}:${name}`)

    const flow = await this.adapter.findOne<FlowDefinition>('flows', { ns, name })

    if (flow) {
      await repoManager.openRepository(flow.ns)
      await repoManager.removeFlow(flow.name)

      return this.adapter.remove('flows', flow.id)
    }

    return false
  }

  async removeByUserNamespace (
    user: UserModel,
    ns: string,
    { repoManager }: RepoManagerContext
  ): Promise<boolean> {
    (this.log as any).info(`Removing Flows by namespace: ${ns}`)

    const flows = await this.adapter.find<FlowModel>('flows', {
      providerId: user.id,
      ns
    })

    if (flows.length) {
      const names = flows.map((flow) => flow.name)

      await repoManager.openRepository(ns)
      await repoManager.removeFlows(names)

      return this.adapter.removeAll('flows', {
        ns,
        providerId: user.id
      })
    }

    return false
  }

  async removeByUserNamespaceAndName (
    user: UserModel,
    ns: string,
    name: string,
    { repoManager }: RepoManagerContext
  ): Promise<boolean> {
    (this.log as any).info(`Removing Flow by namespace and name: ${ns}:${name}`)
    const flow = await this.adapter.findOne<FlowDefinition>('flows', {
      providerId: user.id,
      ns,
      name
    })

    if (flow) {
      await repoManager.removeFlow(flow.name)

      return this.adapter.remove('flows', flow.id)
    }

    return false
  }

  async flush () {
    return this.adapter.flush('flows')
  }

  async close () {
    return this.adapter.close()
  }
}

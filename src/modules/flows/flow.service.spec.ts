import { assert, expect } from 'chai'
import { UserModel } from '@chix/common'
import { FlowService } from './flow.service'

import { email as flow } from './__mocks__/email'
import { ContextModel } from '@nestling/context'
import { createTestApp, TestContext } from '../../support/createTestApp'
import { LogService } from '@nestling/logger'
import { ProjectService } from '../projects'

const context = new ContextModel()

describe('Flow Service', () => {
  let flowService: FlowService
  let testApp: TestContext
  let projectService: ProjectService

  before(async () => {
    testApp = await createTestApp('flows')

    const logger = testApp.app.get(LogService)

    logger.info('Test App created')

    projectService = testApp.app.get(ProjectService)

    // Flushes *all* projects.
    await projectService.flush({
      repoManager: testApp.repoManager
    }, {})

    await projectService.create(testApp.user, {
      name: 'mail',
      description: 'test mail project'
    }, {
      repoManager: testApp.repoManager
    })

    flowService = testApp.app.get(FlowService)

    await flowService.flush()
  })

  after(async () => {
    await projectService.flush({
      repoManager: testApp.repoManager
    }, {})

    if (flowService) {
      flowService.close()
    }

    await testApp.close()
  })

  it('ensures text index', async () => {
    await flowService.ensureIndex()
  })

  it('creates new flow', async () => {
    const _flow = await flowService.create(testApp.user, flow, {
      repoManager: testApp.repoManager
    })

    assert.isOk(_flow.id)
    expect(_flow.provider).to.eql({
      id: testApp.user.id,
      name: testApp.user.name
    })

    delete _flow.providerId

    expect(_flow).to.deep.equal({
      ...flow,
      provider: {
        id: testApp.user.id,
        name: testApp.user.name
      }
    })
  })

  it('finds all flows', async () => {
    const flows = await flowService.find(context)

    assert.isOk(flows.length === 1)
    assert.isOk(flows[0].id)
  })

  it('finds flow by namespace', async () => {
    const flows = await flowService.findByNamespace(context, 'mail')

    assert.isOk(flows.length === 1)
    assert.isOk(flows[0].id)
  })

  it('finds flow by user and namespace', async () => {
    const flows = await flowService.findByUserNamespace(
      context,
      testApp.user,
      'mail'
    )

    assert.isOk(flows.length === 1)
    assert.isOk(flows[0].id)
  })

  it('finds flow by namespace and name', async () => {
    const flow = await flowService.findByNamespaceAndName('mail', 'service')

    assert.isOk(flow.id)
  })

  it('finds flow by user, namespace and name', async () => {
    const flow = await flowService.findByUserNamespaceAndName(
      testApp.user,
      'mail',
      'service'
    )

    assert.isOk(flow.id)
  })

  it('updates flow', async () => {
    const flow = await flowService.findByUserNamespaceAndName(
      testApp.user,
      'mail',
      'service'
    )

    const updated = await flowService.update(
      testApp.user, {
        ...flow,
        name: 'service2',
        title: 'Some Title'
      }, {
        repoManager: testApp.repoManager
      })

    expect(updated.ns).to.equal('mail')
    expect(updated.name).to.equal('service2')
    expect(updated.title).to.equal('Some Title')
  })

  it('removes flow', async () => {
    const flow = await flowService.findByUserNamespaceAndName(
      testApp.user,
      'mail',
      'service2'
    )

    const removed = await flowService.removeByUserNamespaceAndName(
      testApp.user,
      flow.ns,
      flow.name, {
        repoManager: testApp.repoManager
      })

    expect(removed).to.equal(true)
  })
})

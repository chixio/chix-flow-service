import { Module } from '@nestjs/common'
import { FlowController } from './flow.controller'
import { FlowService } from './flow.service'
import { flowErrors } from './flow.errors'
import { ErrorMessage } from '@nestling/errors'
import { SharedModule } from '../../shared/shared.module'
import { DatabaseAdapterModule } from '../../adapter'
import { ContextModule } from '@nestling/context'
import { LoggerModule } from '@nestling/logger'
import { UserModule } from '../../user/user.module'

ErrorMessage.addErrorMessages(flowErrors)

@Module({
  imports: [
    SharedModule,
    LoggerModule,
    DatabaseAdapterModule,
    ContextModule,
    UserModule
  ],
  controllers: [
    FlowController
  ],
  providers: [
    FlowService
  ],
  exports: [
    FlowService
  ]
})
export class FlowModule {}

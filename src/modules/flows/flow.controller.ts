import { Flow as FlowDefinition } from '@chix/common'
import {
  Body,
  Controller,
  Get,
  Delete,
  HttpStatus,
  HttpCode,
  Param,
  Post,
  Put, Query, Head, Res
} from '@nestjs/common'
import { Context } from '@nestling/context'
import { FlowService } from './flow.service'
import { User } from '../../user'
import { Page, Pagination } from '../../paginate'
import { publicOrOwnFilter } from '../shared'
import { UserService } from '../../user/user.service'
import { ErrorMessage } from '@nestling/errors'
import { getRepoManager } from '../../git/getRepoManager'
import { SharedService } from '../../shared/shared.service'

@Controller('flows')
export class FlowController {
  constructor (
    private readonly flowService: FlowService,
    private readonly sharedService: SharedService,
    private readonly userService: UserService
  ) {}
  @Get('')
  @HttpCode(HttpStatus.OK)
  async find (
    @Context() context,
    @User() user,
    @Page() page: Pagination
  ): Promise<FlowDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.flowService.find(
      context,
      { $or },
      {
        ...page
      }
    )
  }

  @Get('search')
  @HttpCode(HttpStatus.OK)
  async search (
    @Context() context,
    @User() user,
    @Query('q') q,
    @Page() page: Pagination
  ): Promise<FlowDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.flowService.search(
      context,
      q,
      { ...page },
      { $or }
    )
  }

  @Get(':provider')
  @HttpCode(HttpStatus.OK)
  async findByUser (
    @Context() context,
    @User() user,
    @Param('provider') providerName,
    @Page() page: Pagination
  ): Promise<FlowDefinition[]> {
    if (user && user.name === providerName) {
      return this.flowService.findByUser(
        context,
        user,
        {},
        { ...page }
      )
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.flowService.findByUser(
      context,
      provider,
      {
        private: {
          $ne: true
        }
      },
      {
        ...page
      }
    )
  }

  @Head(':provider/:ns')
  async userNamespaceExists (
    @User() user,
    @Param('provider') providerName,
    @Param('ns') ns,
    @Res() response
  ) {
    let exists

    if (user && user.name === providerName) {
      exists = await this.flowService.exists(
        user,
        { ns }
      )
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.flowService.exists(
        provider,
        {
          ns,
          private: {
            $ne: true
          }
        }
      )
    }

    if (exists) {
      return response.status(HttpStatus.OK).send()
    }

    return response.status(HttpStatus.NOT_FOUND).send()
  }

  @Get(':provider/:ns')
  @HttpCode(HttpStatus.OK)
  async findByUserNamespace (
    @Context() context,
    @User() user,
    @Page() page: Pagination,
    @Param('provider') providerName,
    @Param('ns') ns
  ): Promise<FlowDefinition[]> {

    if (user && user.name === providerName) {
      return this.flowService.findByUserNamespace(
        context,
        user,
        ns,
        {},
        { ...page }
      )
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.flowService.findByUserNamespace(
      context,
      provider,
      ns,
      {
        private: {
          $ne: true
        }
      },
      { ...page }
    )
  }

  @Head(':provider/:ns/:name')
  async userNamespaceAndNameExists (
    @User() user,
    @Param('provider') providerName,
    @Param('ns') ns,
    @Param('name') name,
    @Res() response
  ) {
    let exists

    if (user && user.name === providerName) {
      exists = await this.flowService.exists(
        user,
        { ns, name }
      )
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.flowService.exists(
        provider,
        {
          ns,
          name,
          private: {
            $ne: true
          }
        }
      )
    }

    if (exists) {
      return response.status(HttpStatus.OK).send()
    }

    return response.status(HttpStatus.NOT_FOUND).send()
  }

  @Get(':provider/:ns/:name')
  @HttpCode(HttpStatus.OK)
  async getByUserNamespaceAndName (
    @User() user,
    @Param('provider') providerName,
    @Param('ns') ns,
    @Param('name') name
  ): Promise<FlowDefinition> {
    if (user && user.name === providerName) {
      return this.flowService.getByUserNamespaceAndName(
        user,
        ns,
        name
      )
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.flowService.findByUserNamespaceAndName(
      provider,
      ns,
      name,
      {
        private: {
          $ne: true
        }
      }
    )
  }

  @Post(':provider')
  @HttpCode(HttpStatus.CREATED)
  async create (
    @User({ required: true }) user,
    @Context() context,
    @Param('provider') provider,
    @Body() flow
  ) {
    const projectExists = await this.sharedService.projectExists(user, {
      name: flow.ns
    })

    if (projectExists) {
      if (user && user.name === provider) {
        const repoManager = getRepoManager(context, provider)

        await repoManager.openRepository(flow.ns)

        let result

        if (Array.isArray(flow)) {
          result = await this.flowService.createMany(user, flow, {
            repoManager
          })
        } else {
          result = await this.flowService.create(user, flow, {
            repoManager
          })
        }

        await repoManager.closeRepository()

        return result
      }

      throw new ErrorMessage('user:invalid')
    }

    throw new ErrorMessage('flow:projectNotFound', { provider, name: flow.ns })
  }

  @Put(':provider')
  @HttpCode(HttpStatus.OK)
  async update (
    @User() user,
    @Context() context,
    @Param('provider') provider,
    @Body() flow
  ) {
    const projectExists = await this.sharedService.projectExists(user, {
      name: flow.ns
    })

    if (projectExists) {
      if (user && user.name === provider) {
        const repoManager = getRepoManager(context, provider)

        await repoManager.openRepository(flow.ns)

        let result

        if (Array.isArray(flow)) {
          result = await this.flowService.updateMany(user, flow, {
            repoManager
          })
        } else {
          result = await this.flowService.update(user, flow, {
            repoManager
          })
        }

        await repoManager.closeRepository()

        return result
      }

      throw new ErrorMessage('user:invalid')
    }

    throw new ErrorMessage('flow:projectNotFound', { provider, name: flow.ns })
  }

  @Delete(':provider/:ns/:name')
  @HttpCode(HttpStatus.NO_CONTENT)
  async removeByUserNamespaceAndName (
    @User() user,
    @Context() context,
    @Param('provider') provider,
    @Param('ns') ns,
    @Param('name') name
  ) {
    if (user && user.name === provider) {
      const repoManager = getRepoManager(context, provider)

      await repoManager.openRepository(ns)

      await this.flowService.removeByUserNamespaceAndName(user, ns, name, {
        repoManager
      })

      await repoManager.closeRepository()
    } else {
      throw new ErrorMessage('user:invalid')
    }
  }
}

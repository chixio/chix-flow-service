import { Collection } from 'mongodb'

import { Injectable } from '@nestjs/common'
import {
  RuntimeDefinition,
  RuntimeModel,
  UserModel
} from '@chix/common'

import { ContextModel } from '@nestling/context'
import { ErrorMessage } from '@nestling/errors'
import { LogService } from '@nestling/logger'
import { ValidatorService } from '@nestling/validator'

import { MongoDbAdapter } from '../../adapter/mongo.adapter'
import { Pagination } from '../../paginate'
import { UserService } from '../../user/user.service'

@Injectable()
export class RuntimeService {
  constructor (
    private log: LogService,
    private adapter: MongoDbAdapter,
    private validatorService: ValidatorService,
    private userService: UserService
  ) {
    this.adapter.addDocFilterFor('runtimes', this.UserFilter.bind(this))
  }

  private async UserFilter<T> (document): Promise<T> {
    if (document.providerId) {
      const user = await this.userService.getUser(document.providerId)

      delete document.providerId

      return {
        ...document as any,
        provider: {
          ...user
        }
      }
    }

    return document
  }

  /**
   * Ensures the text search index exists.
   *
   * Used to do text search during the search operation.
   *
   * @returns {Promise<void>}
   */
  async ensureIndex () {
    const collection: Collection = await this.adapter.db.collection('runtimes')

    try {
      await collection.createIndex({
        label: 'text',
        description: 'text'
      }, {
        name: 'text_search_index'
      })

      this.log.info('Created text_search_index.')
    } catch (e) {}
  }

  async getByUserAndLabel (
    user: UserModel,
    label: string,
    where: any = {}
  ): Promise<RuntimeModel> {
    return this.adapter.findOne<RuntimeModel>('runtimes', {
      providerId: user.id,
      label,
      ...where
    })
  }

  async getById (id: string): Promise<RuntimeModel> {
    return this.adapter.findOne<RuntimeModel>('runtimes',{ id })
  }

  async getByUserAndId (
    user: UserModel,
    id: string
  ): Promise<RuntimeModel> {
    const runtime = await this.adapter.findOne<RuntimeModel>(
      'runtimes',
      {
        providerId: user.id,
        id
      }
    )

    if (!runtime) {
      throw new ErrorMessage('runtime:notFound')
    }

    if (runtime._id) delete runtime._id

    return runtime
  }

  async find (
    context: ContextModel,
    where = {},
    options?: Pagination
  ): Promise<RuntimeModel[]> {
    return this.adapter.find<RuntimeModel>(
      'runtimes',
      where,
      options,
      context
    )
  }

  async search (
    context: ContextModel,
    term: string,
    options?: Pagination,
    where: any = {}
  ): Promise<RuntimeModel[]> {
    return this.adapter.search<RuntimeModel>(
      context,
      'runtimes',
      term,
      options,
      where
    )
  }

  async findByUser (
    context: ContextModel,
    user: UserModel,
    where = {},
    options?: Pagination
  ): Promise<RuntimeModel[]> {
    return this.adapter.find<RuntimeModel>(
      'runtimes', {
        providerId: user.id,
        ...where
      },
      options,
      context
    )
  }

  async create (
    user: UserModel,
    runtime: RuntimeDefinition
  ): Promise<RuntimeModel> {
    await this.validatorService.validate('runtime', runtime)

    ; (this.log as any).info(`Creating Runtime ${runtime.label}`)

    const found = await this.getByUserAndLabel(user, runtime.label)

    if (found) {
      throw new ErrorMessage('runtime:alreadyExists')
    }

    const result = await this.adapter.insert<RuntimeModel>('runtimes', {
      ...runtime,
      registered: new Date(),
      seen: new Date(),
      providerId: user.id
    })

    ;(this.log as any).info(`Created runtime: ${result.label}`)

    if (result._id) delete result._id

    return result
  }

  async createMany (
    user: UserModel,
    runtimes: RuntimeDefinition[]
  ): Promise<RuntimeModel[]> {
    const runtimeModels: RuntimeModel[] = []

    for (const runtime of runtimes) {
      runtimeModels.push(await this.create(user, runtime))
    }

    return runtimeModels
  }

  async update (
    user: UserModel,
    runtime: RuntimeModel
  ): Promise<RuntimeModel> {
    (this.log as any).info(`Update Runtime ${runtime.label}`)

    await this.validatorService.validate('runtime', runtime)

    let found

    if (runtime.id) {
      found = await this.getByUserAndId(user, runtime.id)

      if (!found) {
        throw new ErrorMessage('runtime:notFound')
      }
    } else {
      found = await this.getByUserAndLabel(user, runtime.label)
    }

    const result = await this.adapter.update<RuntimeModel>('runtimes', {
      ...runtime,
      seen: new Date(),
      id: found ? found.id : undefined
    })

    ; (this.log as any).info(`Update runtime ${runtime.label}`)

    return result
  }

  async updateMany (
    user: UserModel,
    runtimes: RuntimeModel[]
  ): Promise<RuntimeModel[]> {
    const runtimeModels: RuntimeModel[] = []

    for (const runtime of runtimes) {
      runtimeModels.push(await this.update(user, runtime))
    }

    return runtimeModels
  }

  async exists (user: UserModel, where: any = {}) {
    const _where: any = {
      providerId: user.id,
      ...where
    }

    ;(this.log as any).info(`Test if exists: ${JSON.stringify(_where)}`)

    return this.adapter.exists('runtimes', _where)
  }

  async removeByUserAndLabel (
    user: UserModel,
    label: string
  ): Promise<boolean> {
    (this.log as any).info(`Removing Runtime: ${user.name}/${label}`)

    const runtime = await this.adapter.findOne<RuntimeModel>('runtimes', {
      providerId: user.id,
      label
    })

    if (runtime) {
      return this.adapter.remove('runtimes', runtime.id)
    }

    return false
  }

  async removeByUserAndId (
    user: UserModel,
    id: string
  ): Promise<boolean> {
    (this.log as any).info(`Removing Runtime: ${user.name}/${id}`)

    const runtime = await this.adapter.findOne<RuntimeModel>('runtimes', {
      providerId: user.id,
      id
    })

    if (runtime) {
      return this.adapter.remove('runtimes', runtime.id)
    }

    return false
  }

  async flush (where: any = {}) {
    const runtimes = await this.adapter.find<RuntimeModel>('runtimes', {
      ...where
    })

    this.log.info(`Removing ${runtimes.length} runtimes`)

    for (const runtime of runtimes) {
      let user
      try {
        user = await this.userService.getUser((runtime as any).provider.id)
      } catch (error) {
        console.log(runtime)
        throw Error(`Owner (${(runtime as any).provider.id}) of runtime ${runtime.label} not found!`)
      }

      await this.removeByUserAndLabel(user, runtime.label)
    }

    return this.adapter.flush('runtimes')
  }

  async close () {
    return this.adapter.close()
  }
}

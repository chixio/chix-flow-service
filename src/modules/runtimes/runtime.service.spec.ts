import { assert, expect } from 'chai'
import { RuntimeService } from './index'
import { ContextModel } from '@nestling/context'
import { createTestApp, TestContext } from '../../support/createTestApp'
import { LogService } from '@nestling/logger'
import { runtime } from './__mocks__/runtime'

const context = new ContextModel()

describe('Runtime Service', () => {
  let runtimeService: RuntimeService
  let testApp: TestContext

  before(async () => {
    testApp = await createTestApp('runtimes')

    const logger = testApp.app.get(LogService)

    logger.info('Test App created')

    runtimeService = testApp.app.get(RuntimeService)

    await runtimeService.flush({})
  })

  after(async () => {
    await runtimeService.flush({})

    const runtimes = await testApp.mongoAdapter.find('runtimes')
    expect(runtimes, 'Runtime cleanup').to.deep.equal([])

    if (runtimeService) {
      runtimeService.close()
    }

    await testApp.close()
  })

  it('ensures text index', async () => {
    await runtimeService.ensureIndex()
  })

  it('creates new runtime', async () => {
    const _runtime = await runtimeService.create(
      testApp.user,
      runtime
    )

    assert.isOk(_runtime.id)
    expect((_runtime as any).provider).to.eql({
      id: testApp.user.id,
      name: testApp.user.name
    })

    expect(_runtime.id).to.be.a('string')
    expect(_runtime.address).to.be.equal('wss://localhost:3569')
    expect(_runtime.protocol).to.be.equal('websocket')
    expect(_runtime.type).to.be.equal('chix')
    expect(_runtime.label).to.be.equal('Default Runtime')
    expect(_runtime.description).to.be.equal('This is the default runtime')
    expect(_runtime.registered).to.be.instanceOf(Date)
    expect(_runtime.seen).to.be.instanceOf(Date)
    expect(_runtime.user).to.equal('rhalff')
    expect(_runtime.secret).to.equal('4571')

    expect(_runtime.provider).to.deep.equal({
      id: testApp.user.id,
      name: testApp.user.name
    })
  })

  it('finds all runtimes', async () => {
    const runtimes = await runtimeService.find(context)

    assert.isOk(runtimes.length === 1)
    assert.isOk(runtimes[0].id)
  })

  it('can find runtime as part of name', async () => {
    const runtimes = await runtimeService.search(context, 'Default')

    assert.isOk(runtimes.length === 1)
    assert.isOk(runtimes[0].id)
  })

  it('can find runtime as part of description', async () => {
    const runtimes = await runtimeService.search(context, 'default runtime')

    assert.isOk(runtimes.length === 1)
    assert.isOk(runtimes[0].id)
  })

  it('can get runtime by user and namespace', async () => {
    const runtime = await runtimeService.getByUserAndLabel(
      testApp.user,
      'Default Runtime'
    )

    assert.isOk(runtime.id)
    expect((runtime as any).provider).to.eql({
      id: testApp.user.id,
      name: testApp.user.name
    })
    expect(runtime.label).to.equal('Default Runtime')
  })

  it('updates runtime', async () => {
    const runtime = await runtimeService.getByUserAndLabel(
      testApp.user,
      'Default Runtime'
    )

    const updated = await runtimeService.update(
      testApp.user, {
        ...runtime,
        label: 'New Runtime'
      })

    expect(updated.label).to.equal('New Runtime')
  })

  it('removes runtime', async () => {
    const runtime = await runtimeService.getByUserAndLabel(
      testApp.user,
      'New Runtime'
    )

    const removed = await runtimeService.removeByUserAndLabel(
      testApp.user,
      runtime.label
    )

    expect(removed).to.equal(true)
  })
})

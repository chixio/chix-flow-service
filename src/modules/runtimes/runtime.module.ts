import { Module } from '@nestjs/common'
import { RuntimeController } from './runtime.controller'
import { RuntimeService } from './runtime.service'
import { runtimeErrors } from './runtime.errors'
import { ErrorMessage } from '@nestling/errors'
import { SharedModule } from '../../shared/shared.module'
import { DatabaseAdapterModule } from '../../adapter'
import { ContextModule } from '@nestling/context'
import { UserModule } from '../../user/user.module'

ErrorMessage.addErrorMessages(runtimeErrors)

@Module({
  imports: [
    SharedModule,
    DatabaseAdapterModule,
    UserModule,
    ContextModule
  ],
  controllers: [
    RuntimeController
  ],
  providers: [
    RuntimeService
  ],
  exports: [
    RuntimeService
  ]
})
export class RuntimeModule {}

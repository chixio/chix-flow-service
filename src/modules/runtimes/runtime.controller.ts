import {
  Body,
  Controller,
  Get,
  Delete,
  HttpStatus,
  HttpCode,
  Param,
  Query,
  Post,
  Put,
  OnModuleInit, Res, Head
} from '@nestjs/common'
import { RuntimeDefinition } from '@chix/common'
import { RuntimeService } from './runtime.service'
import { User } from '../../user'
import { Page, Pagination } from '../../paginate'
import { Context } from '@nestling/context'
import { ErrorMessage } from '@nestling/errors'
import { UserService } from '../../user/user.service'
import { publicOrOwnFilter } from '../shared'

@Controller('runtimes')
export class RuntimeController implements OnModuleInit {
  constructor (
    private readonly runtimeService: RuntimeService,
    private readonly userService: UserService
  ) {}

  onModuleInit () {
    console.log('RuntimeController, trying to ensure index')
    return this.runtimeService.ensureIndex()
  }

  /**
   * Returns *all* runtimes.
   *
   * @returns {Promise<[]>}
   */
  @Get('')
  @HttpCode(HttpStatus.OK)
  async find (
    @Context() context,
    @User() user,
    @Page() page: Pagination
  ): Promise<RuntimeDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.runtimeService.find(
      context,
      { $or },
      { ...page }
    )
  }

  /**
   * Search for runtimes.
   *
   * @param q
   * @returns {Promise<[]>}
   */
  @Get('search')
  @HttpCode(HttpStatus.OK)
  async search (
    @Context() context,
    @User() user,
    @Query('q') q,
    @Page() page: Pagination
  ): Promise<RuntimeDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.runtimeService.search(
      context,
      q,
      { ...page },
      { $or }
    )
  }

  /**
   * Get a list of all runtimes for this user.
   *
   * (Flowhub compat)
   *
   * @param user
   * @returns {Promise<[]>}
   */
  @Get()
  @HttpCode(HttpStatus.OK)
  async list (
    @Context() context,
    @User({ required: true }) user
  ): Promise<RuntimeDefinition[]> {
    return this.runtimeService.findByUser(
      context,
      user
    )
  }

  /**
   * Get a list of all runtimes for this user.
   *
   * In case these are the user's own runtimes it will list also include private.
   *
   * @param user
   * @returns {Promise<[]>}
   */
  /*
  @Get(':provider')
  @HttpCode(HttpStatus.OK)
  async findByProvider (
    @Context() context,
    @User() user,
    @Param('provider') providerName,
    @Page() page: Pagination
  ): Promise<RuntimeDefinition[]> {
    if (user && user.name === providerName) {
      return this.runtimeService.findByUser(
        context,
        user,
        {},
        { ...page }
      )
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.runtimeService.findByUser(
      context,
      provider,
      {
        private: {
          $ne: true
        }
      },
      {
        ...page
      }
    )
  }
  */

  @Head(':provider/:name')
  @HttpCode(HttpStatus.OK)
  async providerRuntimeExists (
    @User() user,
    @Param('provider') providerName,
    @Param('name') name,
    @Res() response
  ) {
    let exists

    if (user && user.name === providerName) {
      exists = await this.runtimeService.exists(
        user,
        { name }
      )
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.runtimeService.exists(
        provider,
        {
          name,
          private: {
            $ne: true
          }
        }
      )
    }

    if (exists) {
      return response.status(HttpStatus.OK)
    }

    return response.status(HttpStatus.NOT_FOUND)
  }
  @Post(':id')
  @HttpCode(HttpStatus.OK)
  async ping (
    @User() user,
    @Param('id') id,
    @Res() response
  ) {
    if (user) {
      const exists = await this.runtimeService.exists(
        user,
        { id }
      )

      if (exists) {
        return response.status(HttpStatus.OK)
      }
    }

    return response.status(HttpStatus.NOT_FOUND)
  }

  /**
   * Retrieve runtime by id.
   *
   * @param id
   * @returns {Promise<>}
   */
  @Get(':id')
  @HttpCode(HttpStatus.OK)
  async getById (
    @Param('id') id,
    @User() user
  ): Promise<RuntimeDefinition> {
    if (user) {
      return this.runtimeService.getByUserAndId(user, id)
    }

    throw new ErrorMessage('user:invalid')
  }

  /**
   * Retrieve runtime by username and runtime name.
   *
   * @param providerName
   * @param user
   * @param name
   * @returns {Promise<>}
   */
  @Get(':provider/:name')
  @HttpCode(HttpStatus.OK)
  async getByProviderAndName (
    @Param('provider') providerName,
    @User() user,
    @Param('name') name
  ): Promise<RuntimeDefinition> {
    if (user && user.name === providerName) {
      return this.runtimeService.getByUserAndLabel(user, name)
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.runtimeService.getByUserAndLabel(
      provider,
      name, {
        private: {
          $ne: true
        }
      })
  }

  /**
   * Create a new runtime
   *
   * @param user
   * @returns {any}
   */
  @Post(':provider')
  @HttpCode(HttpStatus.CREATED)
  async create (
    @User({ required: true }) user,
    @Param('provider') provider,
    @Body() runtime
  ) {
    if (user && user.name === provider) {
      return this.runtimeService.create(user, runtime)
    }

    throw new ErrorMessage('user:invalid')
  }

  /**
   * Register given runtime for this user.
   *
   * (FlowHub Compat)
   *
   * @param user
   * @param provider
   * @param runtime
   * @returns {any}
   */
  @Put()
  @HttpCode(HttpStatus.OK)
  async register (
    @User({ required: true }) user,
    @Body() runtime
  ) {
    if (user) {
      let result

      if (Array.isArray(runtime)) {
        result = await this.runtimeService.createMany(user, runtime)
      } else {
        result = await this.runtimeService.create(user, runtime)
      }

      return result
    }

    throw new ErrorMessage('user:invalid')
  }

  /**
   * Update given runtime for this user.
   *
   * @param user
   * @param provider
   * @param runtime
   * @returns {any}
   */
  @Put(':provider')
  @HttpCode(HttpStatus.OK)
  async update (
    @User({ required: true }) user,
    @Param('provider') provider,
    @Body() runtime
  ) {
    if (user && user.name === provider) {
      let result

      if (Array.isArray(runtime)) {
        result = await this.runtimeService.updateMany(user, runtime)
      } else {
        result = await this.runtimeService.update(user, runtime)
      }

      return result
    }

    throw new ErrorMessage('user:invalid')
  }
  /**
   * Remove runtime by user and id.
   *
   * @param id
   * @returns {Promise<void>}
   */
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async removeById (
    @User({ required: true }) user,
    @Param('id') id
  ) {
    if (user) {
      await this.runtimeService.removeByUserAndId(user, id)
    } else {
      throw new ErrorMessage('user:invalid')
    }
  }

  /**
   * Remove runtime by user and label.
   *
   * @param user
   * @param provider
   * @param name
   * @returns {Promise<void>}
   */
  @Delete(':provider/:name')
  @HttpCode(HttpStatus.NO_CONTENT)
  async remove (
    @User({ required: true }) user,
    @Param('provider') provider,
    @Param('name') name
  ) {
    if (user && user.name === provider) {
      await this.runtimeService.removeByUserAndLabel(user, name)
    } else {
      throw new ErrorMessage('user:invalid')
    }
  }
}

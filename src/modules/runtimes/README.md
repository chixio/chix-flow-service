# Runtime Module 

The runtime module allows for registration of runtimes with https://chix.io 

It's compatible with flowhub's api for registering runtimes.

The fbpx cli makes it possible to register your runtime with both chix.io and flowhub.

It's a simple method of notifying each platform of the location of available runtimes.

At the moment chix.io only supports runtimes which are accessible through the websockets protocol.

Registering the runtime is necessary in order to make the runtimes show up on the given platform.

You can register as many runtimes as you want, even if they are not always available.

A runtime can be seen as the host of any arbitrary running graph.

It is a location where graphs can be deployed and accessed through the fbp protocol as defined by noflo.

### Access to the runtime using fbpx cli

First login to chix.io (If not already done):
```
$ fbpx login
Note: the retrieved api key will be written to /home/rhalff/.config/chix/config.json, your credentials will not be stored.
? Username: rhalff
? Password: *******
```

If you would also like to use flowhub you can login using:
```
$ fbpx flowhub --login
Note: the retrieved api key will be written to /home/rhalff/.config/chix/config.json.
? Flowhub User Id: e37b05fe-8g02-378a-99e3-d84252124a20
? Github Personal Access Token: 4xllx067bxl092mcx63231fu84242ex49xcxf9bc
info Flowhub credentials have been stored.
```

Use `fbpx runtime` to manage the runtimes:
```
$ fbpx runtime --help

  Usage: runtime [options]

  Options:
    -r, --register         register with flowhub.
    -c, --clear            clear all runtimes registered.
    -l, --list             list all runtimes registered.
    -d, --del <labelOrId>  remove a runtime registered by runtime label or id.
    -a, --all              register with chix.io and flowhub.
    -f, --flowhub          register with flowhub.
    -h, --help             output usage information
```


import { ProjectDefinition } from '@chix/common'
import {
  Body,
  Controller,
  Get,
  Delete,
  HttpStatus,
  HttpCode,
  Param,
  Query,
  Post,
  Put,
  OnModuleInit, Res, Head
} from '@nestjs/common'
import { ProjectService } from './project.service'
import { User } from '../../user'
import { Page, Pagination } from '../../paginate'
import { Context } from '@nestling/context'
import { ErrorMessage } from '@nestling/errors'
import { UserService } from '../../user/user.service'
import { publicOrOwnFilter } from '../shared'
import { getRepoManager } from '../../git/getRepoManager'

@Controller('projects')
export class ProjectController implements OnModuleInit {
  constructor (
    private readonly projectService: ProjectService,
    private readonly userService: UserService
  ) {}

  onModuleInit () {
    console.log('ProjectController, trying to ensure index')
    return this.projectService.ensureIndex()
  }

  /**
   * Returns *all* projects.
   *
   * @returns {Promise<[]>}
   */
  @Get('')
  @HttpCode(HttpStatus.OK)
  async find (
    @Context() context,
    @User() user,
    @Page() page: Pagination
  ): Promise<ProjectDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.projectService.find(
      context,
      { $or },
      { ...page }
    )
  }

  /**
   * Search for projects.
   *
   * @param q
   * @returns {Promise<[]>}
   */
  @Get('search')
  @HttpCode(HttpStatus.OK)
  async search (
    @Context() context,
    @User() user,
    @Query('q') q,
    @Page() page: Pagination
  ): Promise<ProjectDefinition[]> {
    const $or: any = publicOrOwnFilter(user)

    return this.projectService.search(
      context,
      q,
      { ...page },
      { $or }
    )
  }

  /**
   * Get a list of all projects for this user.
   *
   * In case these are the user's own projects it will list also include private.
   *
   * @param user
   * @returns {Promise<[]>}
   */
  @Get(':provider')
  @HttpCode(HttpStatus.OK)
  async findByProvider (
    @Context() context,
    @User() user,
    @Param('provider') providerName,
    @Page() page: Pagination
  ): Promise<ProjectDefinition[]> {
    if (user && user.name === providerName) {
      return this.projectService.findByUser(
        context,
        user,
        {},
        { ...page }
      )
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.projectService.findByUser(
      context,
      provider,
      {
        private: {
          $ne: true
        }
      },
      {
        ...page
      }
    )
  }

  @Head(':provider/:name')
  @HttpCode(HttpStatus.OK)
  async providerProjectExists (
    @User() user,
    @Param('provider') providerName,
    @Param('name') name,
    @Res() response
  ) {
    let exists

    if (user && user.name === providerName) {
      exists = await this.projectService.exists(
        user,
        { name }
      )
    } else {
      const provider = await this.userService.getUserByName(providerName)

      exists = await this.projectService.exists(
        provider,
        {
          name,
          private: {
            $ne: true
          }
        }
      )
    }

    if (exists) {
      return response.status(HttpStatus.OK)
    }

    return response.status(HttpStatus.NOT_FOUND)
  }

  /**
   * Retrieve project by username and project name.
   *
   * @param providerName
   * @param user
   * @param name
   * @returns {Promise<>}
   */
  @Get(':provider/:name')
  @HttpCode(HttpStatus.OK)
  async getByProviderAndName (
    @Param('provider') providerName,
    @User() user,
    @Param('name') name
  ): Promise<ProjectDefinition> {
    if (user && user.name === providerName) {
      return this.projectService.getByUserAndName(user, name)
    }

    const provider = await this.userService.getUserByName(providerName)

    return this.projectService.getByUserAndName(
      provider,
      name, {
        private: {
          $ne: true
        }
      })
  }

  /**
   * Create a new project
   *
   * @param user
   * @returns {any}
   */
  @Post(':provider')
  @HttpCode(HttpStatus.CREATED)
  async create (
    @User({ required: true }) user,
    @Context() context,
    @Param('provider') provider,
    @Body() project
  ) {
    if (user && user.name === provider) {
      const repoManager = getRepoManager(context, provider)

      const result = await this.projectService.create(user, project, {
        repoManager
      })

      await repoManager.closeRepository()

      return result
    }

    throw new ErrorMessage('user:invalid')
  }

  /**
   * Update given project for this user.
   *
   * Note: nodes belonging to this project are updated at /nodes/:user
   *
   * @param user
   * @param provider
   * @param project
   * @returns {any}
   */
  @Put(':provider')
  @HttpCode(HttpStatus.OK)
  async update (
    @User({ required: true }) user,
    @Context() context,
    @Param('provider') provider,
    @Body() project
  ) {
    if (user && user.name === provider) {
      const repoManager = getRepoManager(context, provider)

      let result

      if (Array.isArray(project)) {
        result = await this.projectService.updateMany(user, project, {
          repoManager
        })
      } else {
        result = await this.projectService.update(user, project, {
          repoManager
        })
      }

      await repoManager.closeRepository()

      return result
    }

    throw new ErrorMessage('user:invalid')
  }

  /**
   * Remove project.
   *
   * @param user
   * @param provider
   * @param name
   * @returns {Promise<void>}
   */
  @Delete(':provider/:name')
  @HttpCode(HttpStatus.NO_CONTENT)
  async remove (
    @User({ required: true }) user,
    @Context() context,
    @Param('provider') provider,
    @Param('name') name
  ) {
    if (user && user.name === provider) {
      const repoManager = getRepoManager(context, provider)

      await repoManager.openRepository(name)

      await this.projectService.removeByUserAndName(user, name, {
        repoManager
      })

      await repoManager.unmountRepository()
    } else {
      throw new ErrorMessage('user:invalid')
    }
  }
}

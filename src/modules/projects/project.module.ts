import { Module } from '@nestjs/common'

import { ContextModule } from '@nestling/context'
import { ErrorMessage } from '@nestling/errors'

import { ProjectController } from './project.controller'
import { ProjectService } from './project.service'
import { projectErrors } from './project.errors'
import { SharedModule } from '../../shared/shared.module'
import { DatabaseAdapterModule } from '../../adapter'
import { UserModule } from '../../user/user.module'
import { NodeModule } from '../nodes'
import { FlowModule } from '../flows'

ErrorMessage.addErrorMessages(projectErrors)

@Module({
  imports: [
    SharedModule,
    DatabaseAdapterModule,
    UserModule,
    NodeModule,
    FlowModule,
    ContextModule
  ],
  controllers: [
    ProjectController
  ],
  providers: [
    ProjectService
  ],
  exports: [
    ProjectService
  ]
})
export class ProjectModule {}

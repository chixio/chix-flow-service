import { Collection } from 'mongodb'

import {
  NodeModel,
  ProjectDefinition,
  ProjectModel,
  UserModel
} from '@chix/common'

import { Injectable } from '@nestjs/common'

import { ContextModel } from '@nestling/context'
import { ErrorMessage } from '@nestling/errors'
import { LogService } from '@nestling/logger'
import { ValidatorService } from '@nestling/validator'

import { MongoDbAdapter } from '../../adapter/mongo.adapter'
import { Pagination } from '../../paginate'
import { UserService } from '../../user/user.service'
import { NodeService, RepoManagerContext } from '../nodes'
import { FlowService } from '../flows'

@Injectable()
export class ProjectService {
  constructor (
    private log: LogService,
    private adapter: MongoDbAdapter,
    private validatorService: ValidatorService,
    private nodeService: NodeService,
    private flowService: FlowService,
    private userService: UserService
  ) {
    this.adapter.addDocFilterFor('projects', this.UserFilter.bind(this))
  }

  private async UserFilter<T> (document): Promise<T> {
    if (document.providerId) {
      const user = await this.userService.getUser(document.providerId)

      delete document.providerId

      return {
        ...document as any,
        provider: {
          ...user
        }
      }
    }

    return document
  }

  /**
   * Ensures the text search index exists.
   *
   * Used to do text search during the search operation.
   *
   * @returns {Promise<void>}
   */
  async ensureIndex () {
    const collection: Collection = await this.adapter.db.collection('projects')

    try {
      await collection.createIndex({
        name: 'text',
        description: 'text'
      }, {
        name: 'text_search_index'
      })

      this.log.info('Created text_search_index.')
    } catch (e) {}
  }

  async getByUserAndName (
    user: UserModel,
    name: string,
    where: any = {}
  ): Promise<ProjectModel> {
    return this.adapter.findOne<ProjectModel>('projects', {
      providerId: user.id,
      name,
      ...where
    })
  }

  async getById (id: string): Promise<ProjectModel> {
    return this.adapter.findOne<ProjectModel>('projects',{ id })
  }

  async getByUserAndId (
    user: UserModel,
    id: string
  ): Promise<ProjectModel> {
    const project = await this.adapter.findOne<ProjectModel>(
      'projects',
      {
        providerId: user.id,
        id
      }
    )

    if (!project) {
      throw new ErrorMessage('project:notFound')
    }

    if (project._id) delete project._id

    return project
  }

  async find (
    context: ContextModel,
    where = {},
    options?: Pagination
  ): Promise<ProjectModel[]> {
    return this.adapter.find<ProjectModel>(
      'projects',
      where,
      options,
      context
    )
  }

  async search (
    context: ContextModel,
    term: string,
    options?: Pagination,
    where: any = {}
  ): Promise<ProjectModel[]> {
    return this.adapter.search<ProjectModel>(
      context,
      'projects',
      term,
      options,
      where
    )
  }

  async findByUser (
    context: ContextModel,
    user: UserModel,
    where = {},
    options?: Pagination
  ): Promise<ProjectModel[]> {
    return this.adapter.find<ProjectModel>(
      'projects', {
        providerId: user.id,
        ...where
      },
      options,
      context
    )
  }

  async create (
    user: UserModel,
    project: ProjectDefinition,
    context: RepoManagerContext
  ): Promise<ProjectModel> {
    await this.validatorService.validate('project', project)

    ; (this.log as any).info(`Creating Project ${project.name}`)

    let nodes

    if (project.nodes) {
      nodes = project.nodes

      delete project.nodes
    }

    const found = await this.getByUserAndName(user, project.name)

    if (found) {
      throw new ErrorMessage('project:alreadyExists')
    }

    await context.repoManager.createRepo(project.name)
    ;(this.log as any).info(`Created Repository ${project.name}`)

    const result = await this.adapter.insert<ProjectModel>('projects', {
      ...project,
      providerId: user.id
    })

    if (nodes) {
      await context.repoManager.openRepository(project.name)
      ;(this.log as any).info(`Opened Repository ${project.name}`)

      for (const node of nodes) {
        if (node.ns !== project.name) {
          throw Error(`Node ${node.ns}:${node.name} does not belong to current project`)
        }

        await this.nodeService.create(user, node as NodeModel, context)
      }

      await context.repoManager.closeRepository()
    }

    (this.log as any).info(`Created project: ${result.name}`)

    if (result._id) delete result._id

    return result
  }

  async createMany (
    user: UserModel,
    projects: ProjectDefinition[],
    context: RepoManagerContext
  ): Promise<ProjectModel[]> {
    const projectModels: ProjectModel[] = []

    for (const project of projects) {
      projectModels.push(await this.create(user, project, context))
    }

    return projectModels
  }

  async update (
    user: UserModel,
    project: ProjectModel,
    context: RepoManagerContext
  ): Promise<ProjectModel> {
    (this.log as any).info(`Update Project ${project.name}`)

    await this.validatorService.validate('project', project)

    let found

    let nodes
    if (project.nodes) {
      nodes = project.nodes

      delete project.nodes
    }

    if (project.id) {
      found = await this.getByUserAndId(user, project.id)

      if (!found) {
        throw new ErrorMessage('project:notFound')
      }

      if (found.name !== project.name) {
        await this.renameRepo(user, found.name, project.name, context)
      }
    } else {
      found = await this.getByUserAndName(user, project.name)

      if (!found) {
        project.providerId = user.id

        await context.repoManager.createRepo(project.name)
      }
    }

    const result = await this.adapter.update<ProjectModel>('projects', {
      ...project,
      id: found ? found.id : undefined
    })

    ; (this.log as any).info(`Update project ${project.name}`)

    if (nodes) {
      await context.repoManager.openRepository(project.name)
      ; (this.log as any).info(`Opened Repository ${project.name}`)

      for (const node of nodes) {
        if (node.ns !== project.name) {
          throw Error(`Node ${node.ns}:${node.name} does not belong to current project`)
        }
        await this.nodeService.update(user, node as NodeModel, context)
      }
    }

    return result
  }

  async renameRepo (user: UserModel, oldName: string, newName: string, context: RepoManagerContext) {
    await context.repoManager.renameRepo(user.name, oldName, newName)
    await context.repoManager.openRepository(newName)

    await this.nodeService.renameNs(user, oldName, newName, context)
    await this.flowService.renameNs(user, oldName, newName, context)

    await context.repoManager.closeRepository()
  }

  async updateMany (
    user: UserModel,
    projects: ProjectModel[],
    context: RepoManagerContext
  ): Promise<ProjectModel[]> {
    const projectModels: ProjectModel[] = []

    for (const project of projects) {
      projectModels.push(await this.update(user, project, context))
    }

    return projectModels
  }

  async exists (user: UserModel, where: any = {}) {
    const _where: any = {
      providerId: user.id,
      ...where
    }

    ;(this.log as any).info(`Test if exists: ${JSON.stringify(_where)}`)

    return this.adapter.exists('projects', _where)
  }

  async removeByUserAndName (
    user: UserModel,
    name: string,
    context: RepoManagerContext
  ): Promise<boolean> {
    (this.log as any).info(`Removing Project: ${user.name}/${name}`)

    const project = await this.adapter.findOne<ProjectModel>('projects', {
      providerId: user.id,
      name
    })

    if (project) {
      await this.nodeService.removeByUserNamespace(user, name, context)
      await this.flowService.removeByUserNamespace(user, name, context)

      try {
        await context.repoManager.deleteRepo(user.name, project.name)
      } catch (error) {
        this.log.error(error)
        this.log.info(`Repository ${user.name}/${project.name} already gone.`)
      }
      return this.adapter.remove('projects', project.id)
    }

    return false
  }

  async flush (context: RepoManagerContext, where: any = {}) {
    const projects = await this.adapter.find<ProjectModel>('projects', {
      ...where
    })

    this.log.info(`Removing ${projects.length} projects`)

    for (const project of projects) {
      let user
      try {
        user = await this.userService.getUser((project as any).provider.id)
      } catch (error) {
        console.log(project)
        throw Error(`Owner (${(project as any).provider.id}) of project ${project.name} not found!`)
      }

      await this.removeByUserAndName(user, project.name, context)
    }

    return this.adapter.flush('projects')
  }

  async close () {
    return this.adapter.close()
  }
}

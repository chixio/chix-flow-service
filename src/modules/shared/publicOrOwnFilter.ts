export function publicOrOwnFilter (user, or: any[] = []) {
  or.push({
    private: { $ne: true }
  })

  if (user) {
    or.push({
      providerId: user.id
    })
  }

  return or
}

import { environment as testEnvironment } from './environment.test'
import { environment as prodEnvironment } from './environment.prod'

let config

if (process.env.ENV === 'test') {
  config = testEnvironment
} else {
  config = prodEnvironment
}

export const environment = config

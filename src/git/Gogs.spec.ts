import { GogsAdminClient } from './GogsAdminClient'
import { GogsClient } from './GogsClient'
import * as objenv from 'objenv'
import { environment } from '../environment/environment.test'
import { expect } from 'chai'
import { generateFakeAccessToken } from '../support'
import { getRepository } from 'typeorm'
import { User } from '../gogs/entities/user.entity'
import { connectDb } from '../support/typeorm/connectDb'

const config = objenv(environment)

describe('GogsClient', () => {
  let adminClient: GogsAdminClient
  let client: GogsClient
  let testUser

  before(async () => {
    adminClient = new GogsAdminClient(
      config.gogs.api.token,
      config.gogs.api.url
    )

    await connectDb()

    const userRepository = getRepository(User)

    testUser = await userRepository.findOne({
      where: {
        name: 'test-user'
      }
    })

    const jwt = await generateFakeAccessToken(
      testUser.id,
      testUser.name,
      config.jwt.secret
    )

    // ok this can be user token OR jwt.
    client = new GogsClient(config.gogs.api.url)
    client.setAuthorization('Bearer', jwt)
    // client.setAuthorization('token', config.test.user.token)
  })

  it('Client can tell if repo does not exists', async () => {
    const result = await client.hasRepo(testUser.name, 'test-repo')

    expect(result).to.eql(false)
  })

  it('User can create repo', async () => {
    await client.createRepo('test-repo', 'My First Chix Repo', true)
  })

  it('Can tell if repo does exists', async () => {
    expect(await client.hasRepo(testUser.name, 'test-repo')).to.eql(true)
  })

  it('User can remove repo', async () => {
    await client.deleteRepo(testUser.name, 'test-repo')
  })

  it('Can tell if repo does not exists', async () => {
    expect(await client.hasRepo(testUser.name, 'test-repo')).to.eql(false)
  })
})

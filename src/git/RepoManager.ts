import { GogsClient } from './GogsClient'
import { GogsAdminClient } from './GogsAdminClient'
import { GitClient } from './GitClient'
import { Flow as FlowDefinition, NodeDefinition } from '@chix/common'
import { LogService } from '@nestling/logger'

export interface RepoManagerOptions {
  apiUrl: string
  cloneDir: string
  gitUrl: string
  token: string
  user: string
  authorization: string,
  logger: LogService
}

export class RepoManager {
  private gogsAdminClient: GogsAdminClient
  private gogsClient: GogsClient
  private gitClient: GitClient
  private logger: LogService

  constructor (options: RepoManagerOptions) {
    const {
      apiUrl,
      token,
      user,
      gitUrl,
      cloneDir,
      authorization,
      logger
    } = options

    this.logger = logger.child({
      class: 'RepoManager'
    })

    this.logger.info('Initialize Gogs Client')

    this.gogsClient = new GogsClient(apiUrl)
    this.gogsClient.setAuthorization(authorization)

    this.logger.info('Initialize Gogs Admin Client')

    this.gogsAdminClient = new GogsAdminClient(apiUrl, token)

    this.logger.info(`Initialize GitClient for user ${user}`)

    this.gitClient = new GitClient({
      user,
      cloneDir,
      gitUrl,
      logger
    })

    this.logger.info('Repomanager initialized.')
  }

  public async openRepository (repo: string) {
    return this.gitClient.openRepository(repo)
  }

  public async closeRepository () {
    return this.gitClient.closeRepository()
  }

  public async unmountRepository () {
    return this.gitClient.unmountRepository()
  }

  public async cleanCloneDir () {
    return this.gitClient.cleanCloneDir()
  }

  ////////////
  // Twigs
  ////////////
  public async getTwig (owner: string, repo: string, name: string, ref: string = 'master') {
    return this.gogsClient.getTwig(owner, repo, name, ref)
  }

  public async hasTwig (owner: string, repo: string, name: string, ref: string = 'master') {
    return this.gogsClient.hasTwig(owner, repo, name, ref)
  }

  public async putTwig (flowDefinition: FlowDefinition) {
    await this.gitClient.putTwig(flowDefinition)
  }

  public async putTwigs (flowDefinitions: FlowDefinition[]) {
    await this.gitClient.putTwigs(flowDefinitions)
  }

  public async removeTwigs (names: string[]) {
    await this.gitClient.removeTwigs(names)
  }

  public async removeTwig (name: string) {
    await this.gitClient.removeTwig(name)
  }

  public async renameTwig (oldName: string, newName: string) {
    await this.gitClient.renameTwig(oldName, newName)
  }

  ////////////
  // Repos
  ////////////
  public async getRepo (owner: string, repo: string) {
    return this.gogsClient.getRepo(owner, repo)
  }

  public async hasRepo (owner: string, repo: string) {
    return this.gogsClient.hasRepo(owner, repo)
  }

  public async getRepos () {
    return this.gogsClient.getRepos()
  }

  public async deleteRepo (owner: string, repo: string) {
    return this.gogsClient.deleteRepo(owner, repo)
  }

  public async clearRepos (owner: string) {
    this.logger.info(`Clear Repositories for user ${owner}`)

    return this.gogsClient.clearRepos(owner)
  }

  public async createRepo (name: string, description?: string, priv: boolean = true) {
    this.logger.info('Creating Repo %s', name)

    return this.gogsClient.createRepo(name, description, priv)
  }

  public async renameRepo (owner: string, oldName: string, newName: string) {
    this.logger.info('Renaming Repo %s:%s -> %s:%s', owner, oldName, owner, newName)

    return this.gogsClient.renameRepo(owner, oldName, newName)
  }

  ////////////
  // Nodes
  ////////////
  public async getNodeDefinition (owner: string, repo: string, name: string, ref: string = 'master') {
    return this.gogsClient.getNodeDefinition(owner, repo, name, ref)
  }

  public async hasNodeDefinition (owner: string, repo: string, name: string, ref: string = 'master') {
    return this.gogsClient.hasNodeDefinition(owner, repo, name, ref)
  }

  public async putNode (nodeDefinition: NodeDefinition) {
    await this.gitClient.putNode(nodeDefinition)
  }

  public async putNodes (nodeDefinitions: NodeDefinition[]) {
    await this.gitClient.putNodes(nodeDefinitions)
  }

  public async removeNodes (names: string[]) {
    await this.gitClient.removeNodes(names)
  }

  public async removeNode (name: string) {
    await this.gitClient.removeNode(name)
  }

  public async renameNode (oldName: string, newName: string) {
    await this.gitClient.renameNode(oldName, newName)
  }

  ////////////
  // Flows
  ////////////
  public async getFlow (owner: string, repo: string, name: string, ref: string = 'master') {
    return this.gogsClient.getFlow(owner, repo, name, ref)
  }

  public async hasFlow (owner: string, repo: string, name: string, ref: string = 'master') {
    return this.gogsClient.hasFlow(owner, repo, name, ref)
  }

  public async putFlow (flowDefinition: FlowDefinition) {
    await this.gitClient.putFlow(flowDefinition)
  }

  public async putFlows (flowDefinitions: FlowDefinition[]) {
    await this.gitClient.putFlows(flowDefinitions)
  }

  public async removeFlows (names: string[]) {
    await this.gitClient.removeFlows(names)
  }

  public async removeFlow (name: string) {
    await this.gitClient.removeFlow(name)
  }

  public async renameFlow (oldName: string, newName: string) {
    await this.gitClient.renameFlow(oldName, newName)
  }
}

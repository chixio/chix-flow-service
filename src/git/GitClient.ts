import { Injectable } from '@nestjs/common'
import { ChixParser } from '@chix/fbpx'
import {
  BaseNodeDefinition,
  Flow as FlowDefinition,
  NodeDefinition
} from '@chix/common'
import { convert as convertToFbpx } from '@chix/flow-tofbpx'
import * as git from 'simple-git/promise'
import * as path from 'path'
import * as fs from 'fs-extra'
import * as _glob from 'glob'
import { promisify } from 'util'
import * as yaml from 'js-yaml'

import { ucfirst } from '../util'
import { LogService } from '@nestling/logger'

const glob = promisify(_glob)

export type StorageFormat = {
  node: 'yml' | 'json',
  flow: 'yml' | 'json' | 'fbp',
  twig: 'yml' | 'json' | 'fbp'
}

export interface GitClientOptions {
  user: string
  gitUrl: string
  logger: LogService
  cloneDir?: string
}

/**
 * Used by autoclean to remove any files which do not match the expected patterns.
 *
 * TODO: enforce this structure with a githook, rejecting any files which do not match the expected fileSystemStructure.
 *
 * @type {RegExp[]}
 */
const fileStructure: RegExp[] = [
  /^nodes\/.*\/node\.(js|ts|json)/,
  /^flows\/.*\.(fbp|yml|json)/,
  /^twigs\/.*\.(fbp|yml|json)/,
  /^package.json/,
  /^x.json/
]
@Injectable()
export class GitClient {
  private cloneDir: string
  private user: string
  private repo: string | null = null
  private isCloned: boolean = false
  private gitUrl: string
  private client
  private logger: LogService
  private storageFormat: StorageFormat = {
    node: 'yml',
    flow: 'fbp',
    twig: 'fbp'
  }

  constructor (options: GitClientOptions) {
    this.user = options.user
    this.gitUrl = options.gitUrl
    this.cloneDir = options.cloneDir || path.resolve(__dirname, '../../tmp')
    this.logger = options.logger.child({
      class: 'GitClient'
    })
  }

  /**
   * Strictly removes all files not belonging to the predefined repository structure
   */
  public async autoclean () {
    await this.openRepository()

    process.chdir(this.targetDir)

    if (process.cwd() !== this.targetDir) {
      throw Error('Refusing to continue')
    }

    const files = await glob(`**/*.*`)

    const filesToBeRemoved = files.filter((file) => {
      return !fileStructure.find((regexp) => {
        return regexp.test(file)
      })
    })

    if (filesToBeRemoved.length) {
      await Promise.all(
        filesToBeRemoved.map((file) => fs.remove(file))
      )

      const client = git(this.targetDir).silent(false)

      await client.add(filesToBeRemoved)

      await client.commit(`[Node][autoclean]`, filesToBeRemoved)

      await client.push('origin', 'master')
    }

    process.chdir(this.cloneDir)
  }

  public async cleanCloneDir () {
    this.logger.info('Removing cloneDir %s', this.cloneDir)

    await fs.remove(this.cloneDir)
  }

  public async openRepository (repo?: string) {
    if (repo) {
      if (this.repo && repo !== this.repo) {
        throw Error(`Cannot open multiple repositories. still open: ${this.repo}, to be opened: ${repo}`)
      }

      this.repo = repo
    }

    if (!this.repo) {
      throw Error('No repository opened.')
    }

    if (!this.isCloned) {
      const remote = `${this.gitUrl}/${this.user}/${this.repo}`
      this.client = git().silent(false)

      await this.closeRepository()

      await this.client.clone(remote, this.targetDir, [
        // '--depth 1'
      ])

      this.client = git(this.targetDir).silent(false)

      this.isCloned = true

      this.logger.info('Opened repository %s', this.repo)
    } else {
      this.logger.info('Re-opened repository %s', this.repo)
    }
  }

  public async closeRepository () {
    if (this.isCloned) {
      const status = await this.client.status()

      if (status.staged.length) {
        this.logger.info(`Pushing ${status.staged.length} files to ${this.repo}`)

        await this.client.push('origin', 'master')
      } else {
        this.logger.info(`No staged files found for ${this.repo}`)

        try {
          await this.client.push('origin', 'master')
          this.logger.info(`Initial push was successful for ${this.repo} `)
        } catch (_error) {}
      }

      await this.unmountRepository()

      this.logger.info('Closed repository %s', this.repo)

      this.repo = null
    }
  }

  public async unmountRepository () {
    const exists = await fs.pathExists(this.targetDir)

    if (exists) {
      this.logger.info(`Removing ${this.targetDir}`)
      await fs.remove(this.targetDir)
    }

    this.isCloned = false
  }

  get targetDir () {
    return `${this.cloneDir}/${this.user}/${this.repo}`
  }

  get nodesDir () {
    return `${this.targetDir}/nodes`
  }

  get flowsDir () {
    return `${this.targetDir}/flows`
  }

  get twigsDir () {
    return `${this.targetDir}/twigs`
  }

  /////////////
  // Nodes
  /////////////
  public async getNode (name: string): Promise<NodeDefinition> {
    await this.openRepository()

    const nodeDir = `${this.nodesDir}/${name}`
    const nodeFile = `${nodeDir}/node`

    return this.readFile('node', nodeFile)
  }

  public async putNode (nodeDefinition: NodeDefinition) {
    return this._putNode<NodeDefinition>('node', nodeDefinition)
  }

  public async putNodes (nodeDefinitions: NodeDefinition[]) {
    return this._putNodes<NodeDefinition>('node', nodeDefinitions)
  }

  public async removeNode (name: string) {
    return this._removeNode('node', name)
  }

  public async removeNodes (names: string[]) {
    return this._removeNodes('node', names)
  }

  public async renameNode (oldName: string, newName: string) {
    return this._renameNode('node', oldName, newName)
  }

  /////////////
  // Flows
  /////////////
  public async getFlow (name: string): Promise<FlowDefinition> {
    await this.openRepository()

    const nodeDir = `${this.flowsDir}/${name}`
    const nodeFile = `${nodeDir}/flow`

    return this.readFile('flow', nodeFile)
  }

  public async putFlow (nodeDefinition: FlowDefinition) {
    return this._putNode<FlowDefinition>('flow', nodeDefinition)
  }

  public async putFlows (nodeDefinitions: FlowDefinition[]) {
    return this._putNodes<FlowDefinition>('flow', nodeDefinitions)
  }

  public async removeFlow (name: string) {
    return this._removeNode('flow', name)
  }

  public async removeFlows (names: string[]) {
    return this._removeNodes('flow', names)
  }

  public async renameFlow (oldName: string, newName: string) {
    return this._renameNode('flow', oldName, newName)
  }

  /////////////
  // Twigs
  /////////////
  public async getTwig (name: string): Promise<FlowDefinition> {
    await this.openRepository()

    const nodeDir = `${this.twigsDir}/${name}`
    const nodeFile = `${nodeDir}/twig`

    return this.readFile('twig', nodeFile)
  }

  public async putTwig (nodeDefinition: FlowDefinition) {
    return this._putNode<FlowDefinition>('twig', nodeDefinition)
  }

  public async putTwigs (nodeDefinitions: FlowDefinition[]) {
    return this._putNodes<FlowDefinition>('twig', nodeDefinitions)
  }

  public async removeTwig (name: string) {
    return this._removeNode('twig', name)
  }

  public async removeTwigs (names: string[]) {
    return this._removeNodes('twig', names)
  }

  public async renameTwig (oldName: string, newName: string) {
    return this._renameNode('twig', oldName, newName)
  }

  ///////////////////////////////////
  // Private Generic Node Methods
  ///////////////////////////////////
  private async _putNode <T extends BaseNodeDefinition> (type: string, nodeDefinition: T) {
    await this.openRepository()

    this.logger.info(`Put ${type} ${nodeDefinition.ns}:${nodeDefinition.name}`)

    const nodeDir = `${this.targetDir}/${type}s/${nodeDefinition.name}`
    const nodeFile = `${nodeDir}/${type}`

    await fs.mkdirp(nodeDir)

    await this.writeFile(type, nodeFile, nodeDefinition)

    const storageFormat = this.getStorageFormat(type)

    await this.client.raw(['add', `${nodeFile}.${storageFormat}`])

    const identifier = `${nodeDefinition.ns}:${nodeDefinition.name}`

    const commitMessage = `[${ucfirst(type)}][add] ${identifier}`

    this.logger.info(commitMessage)

    await this.client.commit(
      commitMessage,
      [`${nodeFile}.${storageFormat}`]
    )
  }

  private async _putNodes <T extends BaseNodeDefinition> (type: string, nodeDefinitions: T[]) {
    await this.openRepository()

    await Promise.all(
      nodeDefinitions.map((nodeDefinition) => this._putNode<T>(type, nodeDefinition))
    )
  }

  // in a repo ns is always considered to be the repo name.
  // Flow *can* include nodes from other namespaces but the noeDefinitions themselves
  // *must* be namespaced to the repo name
  // the name can have slashes in them, which will cause grouping e.g. header/button
  public async _removeNode (type: string, name: string) {
    await this.openRepository()

    const parts = name.split('/')

    const nodeDir = path.resolve(`${this.targetDir}/${type}s`, ...parts)

    const exists = await fs.pathExists(nodeDir)

    if (exists) {
      await fs.remove(nodeDir)

      await this.client.add([nodeDir])

      await this.client.commit(`[${ucfirst(type)}][remove] ${name}`, [nodeDir])
    }
  }

  public async _removeNodes (type: string, names: string[]) {
    await this.openRepository()

    await Promise.all(
      names.map((name) => this._removeNode(type, name))
    )
  }

  public async _renameNode (type: string, oldName: string, newName: string) {
    await this.openRepository()

    this.logger.info(`Rename ${type}: ${oldName} > ${newName}`)

    const oldParts = oldName.split('/')
    const newParts = newName.split('/')

    const oldNodeDir = path.resolve(`${this.targetDir}/${type}s`, ...oldParts)
    const newNodeDir = path.resolve(`${this.targetDir}/${type}s`, ...newParts)

    this.logger.info(`Moving ${oldNodeDir} > ${newNodeDir}`)

    const oldNodeExists = await fs.pathExists(oldNodeDir)

    if (oldNodeExists) {
      await this.client.mv(oldNodeDir, newNodeDir)

      await this.client.commit(
        `[${ucfirst(type)}][move] ${oldName} -> ${newName}`,
        [oldNodeDir, newNodeDir]
      )
    }
  }

  ////////////////////////////////////////////////////////

  private getStorageFormat (type) {
    if (this.storageFormat.hasOwnProperty(type)) {
      return this.storageFormat[type]
    }

    throw Error(`Unknown type: ${type}`)
  }

  public async tag (version: string) {
    await this.openRepository()

    await this.client.addTag(version)
    await this.client.pushTags('origin')
  }

  private toYaml (nodeDefinition: any) {
    return yaml.safeDump(nodeDefinition, {
      indent: 2,
      skipInvalid: false,
      flowLevel: Infinity
      // schema: (yaml as any).DEFAULT_SAFE_SCHEMA
    })
  }

  private fromYaml (contents) {
    return yaml.safeLoad(contents)
  }

  private async readFile (type: string, file: string) {
    let contents

    switch (this.getStorageFormat(type)) {
      case 'json':
        contents = await fs.readJSON(`${file}.json`)
        break
      case 'yml':
        contents = this.fromYaml(await fs.readFile(`${file}.yml`))
        break
      case 'fbp':
        contents = ChixParser({
          defaultProvider: `https://api.chix.io/v1/nodes/${this.user}/{ns}/{name}`
        }).parse(await fs.readFile(`${file}.fbp`))
        break
      default:
        throw Error('Unknown storage format.')
        break
    }

    return contents
  }

  private async writeFile (type, file, nodeDefinition) {
    switch (this.getStorageFormat(type)) {
      case 'json':
        await fs.writeJSON(`${file}.json`, nodeDefinition)
        break
      case 'yml':
        await fs.writeFile(`${file}.yml`, this.toYaml(nodeDefinition))
        break
      case 'fbp':
        await fs.writeFile(`${file}.fbp`, convertToFbpx(nodeDefinition))
        break
      default:
        throw Error('Unknown storage format.')
        break
    }
  }
}

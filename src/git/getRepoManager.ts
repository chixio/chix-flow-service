import { RepoManager } from './RepoManager'
import { environment } from '../environment/environment'
import * as objenv from 'objenv'
import * as assert from 'assert'
import { ContextService } from '@nestling/context'

export function getRepoManager (context: ContextService, user: string) {
  const config: any = objenv(environment)

  const authorization = context.get('Authorization')
  const logger = context.get('logger')

  assert(logger, 'Logger not found')
  assert(authorization, 'Authorization header not found')

  const {
    host,
    port,
    user: gitUser
  } = config.gogs.git

  const gitUrl = `ssh://${gitUser}@${host}:${port}`

  console.log('gitUrl', gitUrl)

  return new RepoManager({
    apiUrl: `${config.gogs.api.url}`,
    cloneDir: config.gogs.git.cloneDir,
    gitUrl,
    token: config.gogs.api.token,
    user,
    authorization,
    logger
  })
}
